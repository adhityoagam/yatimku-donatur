<!-- The file upload form used as target for the file upload widget -->
<?php if ($this->showForm) echo CHtml::beginForm($this -> url, 'post', $this -> htmlOptions);?>

	<div class="rows">
		<?php 
			$modul = explode("/", Yii::app()->urlManager->parseUrl(Yii::app()->request));
			if($modul[0] == "paketdonasi"){
				$object_name = 'paketdonasi';
				$fFolder = "paketdonasi";}
				
			if(isset($modul[1])){
				if($modul[1] == "update"){
					$models = Yii::app()->db->createCommand()->select('id, object_id, name, filename, object_name, caption_title, caption_desc, type')->from('files')->where('object_name=:ggrks AND object_id=:ggrksu', array(':ggrks'=>$object_name, ':ggrksu'=>Yii::app()->request->getParam('id')))->queryAll();
				}
				elseif($modul[1] == "index"){
					$models = Yii::app()->db->createCommand()->select('id, name, filename, object_name, caption_title, caption_desc, type')->from('files')->where('object_name=:ggrks', array(':ggrks'=>$object_name))->queryAll();
				}
				else{
					$models = Yii::app()->db->createCommand()->select('id, object_id, name, filename, object_name, caption_title, caption_desc, type')->from('files')->where('object_name=:ggrks AND object_id=:ggrksu AND user_id=:awaw', array(':ggrks'=>$object_name, ':ggrksu'=>0, ':awaw'=>Yii::app()->user->id))->queryAll();
				}
			}
			else{
				$models = Yii::app()->db->createCommand()->select('id, name, filename, object_name, caption_title, caption_desc, type')->from('files')->where('object_name=:ggrks', array(':ggrks'=>$object_name))->queryAll();
			}
			if(count($models) > 0):
				foreach($models as $model):?>
					<div class="col-md-12 <?php echo $modul[0]; ?> just-once" style="margin-bottom:5px;overflow:hidden;" data-diff="<?php echo $model['id']."-".$model['object_name']; ?>">
						<div class="row">
							<hr />
							<div class="row" style="margin-bottom:5px;overflow:hidden;">
								<span class="col-md-12" style="word-wrap:break-word;">
									<?php echo $model['name']; ?>
								</span>
							</div>
							<div class="row" style="margin-bottom:5px;overflow:hidden;">
								<div class="col-md-5 preview">
									<a href="<?php echo Yii::app( )->getBaseUrl( )."/uploads/{$fFolder}/".$model['filename']; ?>" title="<?php echo $model['name']; ?>" rel="gallery" download="<?php echo $model['filename'];?>">
										<?php 
											$isImage = false; 
											$mimeArr = array('image/jpeg', 'image/jpg', 'image/gif', 'image/bmp', 'image/png'); 
											if(in_array($model['type'], $mimeArr)){$isImage = true;} ?>
										<?php if($isImage): ?>
										<img style="width:100%;" src="<?php echo Yii::app( )->getBaseUrl( )."/uploads/{$fFolder}/".$model['filename']; ?>" />
										<?php else: ?>
											<i class="fs32 fa fa-film fa-5x"></i>
										<?php endif; ?>
									</a>
								</div>
								<div class="col-md-7">
									<strong>Title</strong>
									<div style="margin-bottom:5px;word-wrap:break-word;">
										<span class="caption-title" data-text="<?php echo $model['caption_title']; ?>"><?php echo $model['caption_title']; ?></span>
									</div>
								</div>
							</div>
							<div>
								<span class="pull-right"> 
									<a class="<?php echo $modul[0]; ?> btn btn-primary edit-caption" data-id="<?php echo $model['id']."-".$model['object_name']; ?>">
										<i class="icon-pencil"></i>
										<span>Edit Title</span>
									</a>
									<a class="<?php echo $modul[0]; ?> btn btn-danger delete-caption" data-id="<?php echo $model['id']."-".$model['object_name']; ?>" data-modul="<?php echo $modul[0]; ?>">
										<i class="icon-trash"></i>
										<span>Delete</span>
									</a>
								</span>
							</div>
						</div>
					</div>
		<?php	endforeach;
			endif;
		?>
		<hr />
	</div>
<div class="fileupload-buttonbar">
	<div class="row">
	<div class="col-md-12">
		<!-- The fileinput-button col-md- is used to style the file input field as button -->
		<span class="btn btn-success fileinput-button <?php if(count($models) >= 2 && $modul[0] == "iklan") echo "disabled"; ?><?php if(count($models) > 0 && $modul[0] == "page") echo "disabled"; ?>">
            <i class="icon-plus icon-white"></i>
            <col-md-><?php echo $this->t('1#Add File|0#Choose File', $this->multiple); ?></col-md->
			<?php
            if ($this -> hasModel()) :
                echo CHtml::activeFileField($this -> model, $this -> attribute, $htmlOptions) . "\n";
            else :
                echo CHtml::fileField($name, $this -> value, $htmlOptions) . "\n";
            endif;
            ?>
		</span>
        <?php if ($this->multiple) { ?>
		<button type="submit" class="btn btn-primary start uploadsors">
			<i class="icon-upload icon-white"></i>
			<span>Upload</span>
		</button>
		<button type="reset" class="btn btn-warning cancel">
			<i class="icon-ban-circle icon-white"></i>
			<span>Cancel</span>
		</button>
		<button type="button" class="btn btn-danger delete">
			<i class="icon-trash icon-white"></i>
			<span>Delete</span>
		</button>
		<input type="checkbox" class="toggle">
        <?php } ?>
	</div>
	</div>
	<div class="col-md-5">
		<!-- The global progress bar -->
		<div class="progress progress-success progress-striped active fade">
			<div class="bar" style="width:0%;"></div>
		</div>
	</div>
</div>
<!-- The loading indicator is shown during image processing -->
<div class="fileupload-loading"></div>
<br>
<!-- The table listing the files available for upload/download -->
<div class="r">
	<table class="table table-condensed col-md-12">
		<tbody class="files" data-toggle="modal-gallery" data-target="#modal-gallery"></tbody>
	</table>
</div>
<?php if ($this->showForm) echo CHtml::endForm();?>
