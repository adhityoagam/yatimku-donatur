<?php
	/**
	 * Yii Excel File Reader (Yexcel) Class
	 * by: Michel Kogan
	 * --------------------
	 * LICENSE
	 * --------------------
	 * This program is open source product; you can redistribute it and/or
	 * modify it under the terms of the GNU General Public License (GPL)
	 * as published by the Free Software Foundation; either version 2
	 * of the License, or (at your option) any later version.
	 * This program is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
	 *
	 * --------------------
	 * @package    Yexcel
	 * @author     Michel Kogan <kogan.michel@gmail.com
	 * @copyright  2012 Michel Kogan
	 * @license    http://www.gnu.org/copyleft/gpl.html  GNU General Public License (GPL)
	 * @link       http://www.sparta.ir
	 * @see        FileSystem
	 * @version    1.0.0
	 *
	 *
	 */
	/** Include path **/
	set_include_path(get_include_path() . PATH_SEPARATOR . Yii::app()->basePath.'/extensions/yexcel/Classes/');

	/** PHPExcel_IOFactory */
	include 'PHPExcel/IOFactory.php';

	class Yexcel
	{
		function __construct()
		{
		}

		public function init()
		{
		}

		public function readActiveSheet( $file )
		{
			$objPHPExcel = PHPExcel_IOFactory::load($file);
			$sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

			return $sheetData;
		}
		
		public function writeSheet( $array, $filename = NULL ){
			if(NULL == $filename)
				$filename = "Export - ".date("Y-m-d H-i-s");
			
			$arrayCount = count($array);
				
			$objPHPExcel = new PHPExcel();
			
			$row = 1;
			foreach($array as $key => $Arr){
				$column = 'A';
				foreach($Arr as $keyArr => $ArrSecond){
					if(is_array($ArrSecond)){
						
						if(!empty($ArrSecond['value']))
							$objPHPExcel->getActiveSheet()->SetCellValue($column.$row, $ArrSecond['value'], PHPExcel_Cell_DataType::TYPE_STRING);
						else
							$objPHPExcel->getActiveSheet()->SetCellValue($column.$row, '', PHPExcel_Cell_DataType::TYPE_STRING);
						
						if(!empty($ArrSecond['font'])){
							$bold = false;
							$color = '000000';
							$size = '11';
							$name = 'Calibri';
							if(!empty($ArrSecond['font']['bold']))
								$bold = true;
							if(!empty($ArrSecond['font']['color']))
								$color = $ArrSecond['font']['color'];
							if(!empty($ArrSecond['font']['size']))
								$size = $ArrSecond['font']['size'];
							if(!empty($ArrSecond['font']['name']))
								$name = $ArrSecond['font']['name'];
								
							$styleArray = array(
								'font'  => array(
									'bold'  => $bold,
									'color' => array('rgb' => $color),
									'size'  => $size,
									'name'  => $name
								)
							);
							$objPHPExcel->getActiveSheet()->getStyle($column.$row)->applyFromArray($styleArray);
						}
						
						if(!empty($ArrSecond['width'])){
							$objPHPExcel->getActiveSheet()->getColumnDimension($column)->setWidth($ArrSecond['width']);
						}else{
							$objPHPExcel->getActiveSheet()->getColumnDimension($column)->setAutoSize(true);
						}
						
						if(!empty($ArrSecond['height']))
							$objPHPExcel->getActiveSheet()->getRowDimension($column)->setRowHeight($ArrSecond['height']);
							
						if(!empty($ArrSecond['background'])){
							$background = 'FFFFFF';
							if(!empty($ArrSecond['background']))
								$background = $ArrSecond['background'];
								
							$objPHPExcel->getActiveSheet()->getStyle($column.$row)->applyFromArray(
								array(
									'fill' => array(
										'type' => PHPExcel_Style_Fill::FILL_SOLID,
										'color' => array('rgb' => $background)
									)
								)
							);
						}
						
						if(!empty($ArrSecond['align'])){
							if($ArrSecond['align'] == 'right')
								$objPHPExcel->getActiveSheet()->getStyle($column.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
								
							if($ArrSecond['align'] == 'left')
								$objPHPExcel->getActiveSheet()->getStyle($column.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
								
							if($ArrSecond['align'] == 'center')
								$objPHPExcel->getActiveSheet()->getStyle($column.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
						}
					}
					else{
						$objPHPExcel->getActiveSheet()->SetCellValue($column.$row, $ArrSecond, PHPExcel_Cell_DataType::TYPE_STRING);
						$objPHPExcel->getActiveSheet()->getColumnDimension($column)->setAutoSize(true);
					}
					$column++;
				}
				$row++;
			}
			
			// $objPHPExcel->getActiveSheet()->fromArray($array, NULL, 'A1');
			
			// Set Column Autosize
			// foreach (range(0, $arrayCount) as $column) {
				// $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($column)->setAutoSize(true);                
			// }
			
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
			$objWriter->save('uploads/anyfile/report/'.$filename.'.xlsx');
		}
	}

?>