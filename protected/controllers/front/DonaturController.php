<?php

class DonaturController extends Controller
{
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','konfirmasi','program','programdetail'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
	
	public function actionKonfirmasi(){
		$model=new Donasi;
		$user_id = 0;
		$user = array(
			'name'=>'',
			'email'=>'',
			'phone'=>'',
		);
		
		if(!Yii::app()->user->isGuest){
			$userModel = User::findByPk(Yii::app()->user->id);
			$userOptions = UserSettings::findByAttributes(array('user_id'=>Yii::app()->user->id));
			$user['email'] = $userModel->email;
			$user['name'] = $userOptions->nama_lengkap;
			$user['phone'] = $userOptions->no_telp;
			$user_id = Yii::app()->user->id;
		}
		
		$rekeningTujuanModel = array();
		$rekeningTujuanModel[''] = 'Pilih Rekening Tujuan';
		$paketDonasiModel[''] = 'Pilih Program Donasi';
		
		$rekeningTujuanModels = Yii::app()->db->createCommand("SELECT id, bank, nama_rekening, nomor_rekening FROM rekening_pembayaran")->queryAll();
		foreach($rekeningTujuanModels as $rekeningTujuanM){
			$rekeningTujuanModelsID = $rekeningTujuanM['id'];
			$rekeningTujuanModel[$rekeningTujuanModelsID] = $rekeningTujuanM['bank']." ".$rekeningTujuanM['nama_rekening']." - ".$rekeningTujuanM['nomor_rekening'];
		}
		
		$paketDonasiModels = Yii::app()->db->createCommand("SELECT id, nama_paket, donasi FROM paket_donasi")->queryAll();
		foreach($paketDonasiModels as $paketDonasiM){
			$paketDonasiModelsID = $paketDonasiM['id'];
			$paketDonasiModel[$paketDonasiModelsID] = $paketDonasiM['nama_paket']." - ".$paketDonasiM['donasi'];
		}

		if(isset($_POST['Donasi']))
		{
			$model->attributes=$_POST['Donasi'];
			$model->user_id = $user_id;
			
			if($model->save())
				$this->redirect(array('index'));
		}

		$this->render('konfirmasi',array(
			'model'=>$model,
			'rekeningTujuanModel'=>$rekeningTujuanModel,
			'paketDonasiModel'=>$paketDonasiModel,
			'userModel'=>$user,
		));
	}
	
	public function actionIndex()
	{
		$this->render('index',array(
			
		));
	}
	
	public function actionProgram(){
		
		$model = PaketDonasi::model()->findAll();
		
		$imageModel = array();
		$imageModels = Yii::app()->db->createCommand("SELECT id, filename, object_id FROM files WHERE object_name = 'paketdonasi'")->queryAll();
		foreach($imageModels as $imageM){
			$imageModelsID = $imageM['object_id'];
			$imageModel[$imageModelsID] = $imageM['filename'];
		}
		
		$this->render('program',array(
			'model'=>$model,
			'imageModel'=>$imageModel,
		));
	}
	
	public function actionProgramdetail($id){
		$model = PaketDonasi::model()->findByPK($id);
		if($model === NULL)
			throw new CHttpException(404,'Maaf, halaman yang anda cari tidak ditemukan.');
			
		$imageModel = array();
		$imageModels = Yii::app()->db->createCommand("SELECT id, filename, object_id FROM files WHERE object_name = 'paketdonasi' AND object_id = :obj_id");
		$imageModels->bindParam(":obj_id", $id, PDO::PARAM_STR);
		$result = $imageModels->queryRow();
		
		$this->render('programdetail',array(
			'model'=>$model,
			'imageModel'=>$result,
		));
	}
	
}