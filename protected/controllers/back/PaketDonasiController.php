<?php

class PaketDonasiController extends Controller
{
	public $layout='//layouts/column2';

	public function filters()
	{
		return array(
			'accessControl',
			'postOnly + delete',
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('index', 'create','update','delete','upload','ajaxeditcaption'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionCreate()
	{
		Yii::import( "xupload.models.XUploadForm" );
		$photos = new XUploadForm;
		$model=new PaketDonasi;

		if(isset($_POST['PaketDonasi']))
		{
			$model->attributes=$_POST['PaketDonasi'];
			
			if($model->save()){
				$user = Yii::app()->user->id;
				$azx = $model->primaryKey;
				$sqlFile = "UPDATE files SET object_id = '$azx' WHERE object_name = 'paketdonasi' AND object_id = 0 AND user_id = '$user'";
				$fileModel = Yii::app()->db->createCommand($sqlFile)->query();
				Yii::app()->user->setFlash("success", "Successfully created");
				$this->redirect(array('index'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
			'photos'=>$photos
		));
	}

	public function actionUpdate($id)
	{
		Yii::import( "xupload.models.XUploadForm" );
		$photos = new XUploadForm;
		$model=$this->loadModel($id);
		
		if(isset($_POST['PaketDonasi']))
		{
			$model->attributes=$_POST['PaketDonasi'];
			
			if($model->save()){				
				$this->redirect(array('index'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
			'photos'=>$photos
		));
	}

	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionIndex()
	{
		$model=new PaketDonasi('search');
		$model->unsetAttributes();
		if(isset($_GET['PaketDonasi']))
			$model->attributes=$_GET['PaketDonasi'];

		$indexPage = 'index';
		if(Yii::app()->user->level > 2)
			$indexPage = 'indexdonatur';
		
		$this->render($indexPage,array(
			'model'=>$model,
		));
	}

	public function loadModel($id)
	{
		$model=PaketDonasi::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	public function actionUpload($id = "") {
		if(Yii::app()->request->isPostRequest){
			Yii::import( "xupload.models.XUploadForm" );
			//Here we define the paths where the files will be stored temporarily
			
			$path = getcwd()."/uploads/paketdonasi/";
			$publicPath = Yii::app( )->getBaseUrl( )."/uploads/paketdonasi/";
			if(!is_dir($path)){
				mkdir($path);
				chmod($path, 0777);
			}
		 
			//This is for IE which doens't handle 'Content-type: application/json' correctly
			header( 'Vary: Accept' );
			if( isset( $_SERVER['HTTP_ACCEPT'] ) 
				&& (strpos( $_SERVER['HTTP_ACCEPT'], 'application/json' ) !== false) ) {
				header( 'Content-type: application/json' );
			} else {
				header( 'Content-type: text/plain' );
			}
		 
			//Here we check if we are deleting and uploaded file
			if( isset( $_GET["_method"] ) ) {
				if( $_GET["_method"] == "delete" ) {
					if( $_GET["file"][0] !== '.' ) {
						$file = $path.$_GET["file"];
						if( is_file( $file ) ) {
							if(is_file( $file )){
								unlink( $file );
							}
							if(is_file( $path."thumbnail/".$_GET["file"] )){
								unlink( $path."thumbnail/".$_GET["file"] );
							}
							$fileModel = Files::model()->findByAttributes(array('filename'=>$_GET["file"], 'object_name'=>'paketdonasi'));
							$fileModel->delete();
						}
					}
					echo json_encode( true );
				}
			} else {
				$model = new XUploadForm;
				$model->file = CUploadedFile::getInstance( $model, 'downloadfile' );
				//We check that the file was successfully uploaded
				if( $model->file !== null ) {
					//Grab some data
					$model->mime_type = $model->file->getType( );
					$model->size = $model->file->getSize( );
					$model->name = $model->file->getName( );
					//(optional) Generate a random name for our file
					$filename = md5( Yii::app( )->user->id.microtime( ).$model->name);
					$filename .= ".".$model->file->getExtensionName( );
					if( $model->validate() ) {
					
						Yii::import("ext.EPhpThumb.EPhpThumb");
						$thumb=new EPhpThumb();
						$thumb->init(); //this is needed
						
						//Move our file to our temporary dir
						//Instead of temporary dir, we moved it to exact dir
						$model->file->saveAs( $path.$filename );
						if(!is_dir($path."thumbnail")){
							mkdir($path."thumbnail");
							chmod( $path."thumbnail", 0644 );
						}
						$thumb->create($path.$filename)->adaptiveResize(265,200)->save($path."thumbnail/".$filename);
						
						$files = new Files;
						$files->type = $model->mime_type;
						$files->name = $model->name;
						$files->filename = $filename;
						$files->object_name = 'paketdonasi';
						$files->caption_title = Yii::app()->request->getPost('caption_title', '');
						$files->object_id = $id;
						$files->user_id = Yii::app()->user->id;
						$files->save();
						$agz = $files->primaryKey;
						chmod( $path.$filename, 0777 );
						
						//Now we need to tell our widget that the upload was succesfull
						//We do so, using the json structure defined in
						// https://github.com/blueimp/jQuery-File-Upload/wiki/Setup
						$imageIs = false;
						$mimeArr = array('image/jpeg', 'image/jpg', 'image/gif', 'image/bmp', 'image/png');
						if(in_array($model->mime_type, $mimeArr)){$imageIs = true;}
						echo json_encode( array( array(
								"name" => $model->name,
								"type" => $model->mime_type,
								"size" => $model->size,
								"isImage"=>$imageIs,
								"url" => $publicPath.$filename,
								"thumbnail_url" => $publicPath."$filename",
								"ctitle"   => Yii::app()->request->getPost('caption_title', ''),
								"files_id"	=> $agz,
								"module_name" => "paketdonasi",
								"delete_url" => $this->createUrl( "upload", array(
									"_method" => "delete",
									"file" => $filename,
									"id"	=> $id,
								) ),
								"delete_type" => "POST"
							) ) );
					} else {
						//If the upload failed for some reason we log some data and let the widget know
						echo json_encode( array( 
							array( "error" => $model->getErrors( 'downloadfile' ),
						) ) );
						Yii::log( "XUploadAction: ".CVarDumper::dumpAsString( $model->getErrors( ) ),
							CLogger::LEVEL_ERROR, "xupload.actions.XUploadAction" 
						);
					}
				} else {
					throw new CHttpException( 500, "Upload failed" );
				}
			}
		}
		else{
			throw new CHttpException(404,"Sorry, the page you're looking for isn't here.");
		}
	}
	public function actionAjaxEditCaption(){
		if(Yii::app()->request->isAjaxRequest){
			if(isset($_POST['editCaption'])){
				$z = $_POST['editCaption'];
				$y = $_POST['editCaptions'];
				$dxd = explode("-", $z);
				$file = Files::model()->findByPk($dxd[0]);
				$file->caption_title = $y;
				$file->save();
				echo json_encode(array('a'=>true));
			}
			elseif(isset($_POST['deleteAjax'])){
				$z = $_POST['deleteAjax'];
				$dxd = explode("-", $z);
				$file = Files::model()->findByPk($dxd[0]);
				if(is_file(getcwd()."/uploads/paketdonasi/".$file->filename)){
					unlink(getcwd()."/uploads/paketdonasi/".$file->filename);
				}
				$file->delete();
				echo json_encode(true);
			}
		}
		else{
			throw new CHttpException(404,'Maaf, halaman yang anda cari tidak ditemukan.');
		}
	}
}
