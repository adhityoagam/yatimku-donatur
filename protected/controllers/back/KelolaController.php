<?php

class KelolaController extends Controller
{
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('logout','error','index'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('login','register'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
	
	public function actionIndex(){
		$this->render('index');
	}
	
	public function actionLogin()
	{
		if(!Yii::app()->user->isGuest)
			$this->redirect(Yii::app()->homeUrl);
			
		$this->layout = 'login';
		$model=new LoginFormBackend;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginFormBackend']))
		{
			$model->attributes=$_POST['LoginFormBackend'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}
	
	public function actionRegister(){
		if(!Yii::app()->user->isGuest)
			$this->redirect(Yii::app()->homeUrl);
		
		$this->layout = "//layouts/register";
		$model = new User;
		$settingsModel = new UserSettings;
		
		if(isset($_POST['User'], $_POST['UserSettings'])){
		
			$model->attributes = $_POST['User'];
			$settingsModel->attributes = $_POST['UserSettings'];
			$model->role_id = 2;
			$model->status = 0;
			
			$valid = $model->validate();
			$valid = $settingsModel->validate() && $valid;
			
			if($valid){
				$emailHash = md5("m0TH3rN4tu123".date('Y-m-d:h-i-s'));
				$passwordHash = md5("W#w1sHY0u60DSp33d");
				$PasswordOptions = [
					'salt'=>$passwordHash
				];
				
				$EmailOptions = [
					'salt'=>$emailHash
				];
				$model->key_token = password_hash($model->password, PASSWORD_DEFAULT, $EmailOptions);
				$model->password = password_hash($model->password, PASSWORD_DEFAULT, $PasswordOptions);
				$model->save(false);
				$settingsModel->user_id = $model->primaryKey;
				$settingsModel->save(false);
				Yii::app()->user->setFlash('success', 'Proses daftar telah berhasil, silakan login.');
				$this->redirect(array('kelola/login'));
			}
		}
		
		$this->render('register',array(
			'model'=>$model,
			'settingsModel'=>$settingsModel,
		));
	
	}

	public function actionLogout()
	{	
		Yii::app()->user->setFlash("success", "Anda berhasil logout");
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
}