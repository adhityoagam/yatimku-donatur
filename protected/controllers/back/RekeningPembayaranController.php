<?php

class RekeningPembayaranController extends Controller
{
	public $layout='//layouts/column2';

	public function filters()
	{
		return array(
			'accessControl',
			'postOnly + delete',
		);
	}

	public function accessRules()
	{
		return array(
			array('allow', 
				'actions'=>array('update','create','delete'),
				'users'=>array('@'),
				'expression'=>'Yii::app()->user->level < 3'
			),
			array('allow', 
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('deny',
				'users'=>array('*'),
			),
		);
	}

	public function actionCreate()
	{
		$model=new RekeningPembayaran;

		
		if(isset($_POST['RekeningPembayaran']))
		{
			$model->attributes=$_POST['RekeningPembayaran'];
			if($model->save())
				$this->redirect(array('index'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}


	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		
		if(isset($_POST['RekeningPembayaran']))
		{
			$model->attributes=$_POST['RekeningPembayaran'];
			if($model->save())
				$this->redirect(array('index'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}


	public function actionView($id)
	{
		$model=$this->loadModel($id);

		$this->render('view',array(
			'model'=>$model,
		));
	}

	
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	
	public function actionIndex()
	{
		$model=new RekeningPembayaran('search');
		$model->unsetAttributes();
		
		$indexPage = 'index';
		if(Yii::app()->user->level > 2)
			$indexPage = 'indexdonatur';
		
		if(isset($_GET['RekeningPembayaran']))
			$model->attributes=$_GET['RekeningPembayaran'];

		$this->render($indexPage,array(
			'model'=>$model,
		));
	}

	
	public function loadModel($id)
	{
		$model=RekeningPembayaran::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
}
