<?php

class DonasiController extends Controller
{
	public $layout='//layouts/column2';

	public function filters()
	{
		return array(
			'accessControl',
			'postOnly + delete',
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('index','create','update','delete'),
				'users'=>array('@'),
			),
			array('deny',
				'users'=>array('*'),
			),
		);
	}

	public function actionCreate()
	{
		$model=new Donasi;
		
		$rekeningTujuanModel = array();
		$paketDonasiModel = array();
		$rekeningTujuanModel[''] = 'Pilih Rekening Tujuan';
		$paketDonasiModel[''] = 'Pilih Paket Donasi';
		
		$rekeningTujuanModels = Yii::app()->db->createCommand("SELECT id, bank, nama_rekening, nomor_rekening FROM rekening_pembayaran")->queryAll();
		foreach($rekeningTujuanModels as $rekeningTujuanM){
			$rekeningTujuanModelsID = $rekeningTujuanM['id'];
			$rekeningTujuanModel[$rekeningTujuanModelsID] = $rekeningTujuanM['bank']." ".$rekeningTujuanM['nama_rekening']." - ".$rekeningTujuanM['nomor_rekening'];
		}
		
		$paketDonasiModels = Yii::app()->db->createCommand("SELECT id, nama_paket, donasi FROM paket_donasi")->queryAll();
		foreach($paketDonasiModels as $paketDonasiM){
			$paketDonasiModelsID = $paketDonasiM['id'];
			$paketDonasiModel[$paketDonasiModelsID] = $paketDonasiM['nama_paket']." - ".$paketDonasiM['donasi'];
		}

		if(isset($_POST['Donasi']))
		{
			$model->attributes=$_POST['Donasi'];
			$model->user_id = Yii::app()->user->id;
			if($_POST['Donasi']['donateOption'] == 2){
				$model->donasi = 0;
			}else{
				$model->id_paket_donasi = 0;
			}
			
			if($model->save())
				$this->redirect(array('index'));
		}

		$this->render('create',array(
			'model'=>$model,
			'rekeningTujuanModel'=>$rekeningTujuanModel,
			'paketDonasiModel'=>$paketDonasiModel,
			'tanggal'=>$tanggalModel,
		));
	}

	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		Yii::app()->controller->isUserItSelf($model->user_id);
		
		$rekeningTujuanModel = array();
		$paketDonasiModel = array();
		$rekeningTujuanModel[''] = 'Pilih Rekening Tujuan';
		$paketDonasiModel[''] = 'Pilih Paket Donasi';
		
		$donateOptionSelected = 1;
		if(!empty($model->id_paket_donasi))
			$donateOptionSelected = 2;
		
		$rekeningTujuanModels = Yii::app()->db->createCommand("SELECT id, bank, nama_rekening, nomor_rekening FROM rekening_pembayaran")->queryAll();
		foreach($rekeningTujuanModels as $rekeningTujuanM){
			$rekeningTujuanModelsID = $rekeningTujuanM['id'];
			$rekeningTujuanModel[$rekeningTujuanModelsID] = $rekeningTujuanM['bank']." ".$rekeningTujuanM['nama_rekening']." - ".$rekeningTujuanM['nomor_rekening'];
		}
		
		$paketDonasiModels = Yii::app()->db->createCommand("SELECT id, nama_paket, donasi FROM paket_donasi")->queryAll();
		foreach($paketDonasiModels as $paketDonasiM){
			$paketDonasiModelsID = $paketDonasiM['id'];
			$paketDonasiModel[$paketDonasiModelsID] = $paketDonasiM['nama_paket']." - ".$paketDonasiM['donasi'];
		}

		if(isset($_POST['Donasi']))
		{
			$model->attributes=$_POST['Donasi'];
			if($model->save()){
				if(isset($_POST['Donasi']['sent_email'])){
					if($_POST['Donasi']['sent_email'] == 1)
						$mailBody = "Hai, ".Yii::app()->user->name.". <br />Konfirmasi anda telah kami baca dan telah kami setujui, silakan tunggu proses selanjutnya.";
					elseif($_POST['Donasi']['sent_email'] == 2)
						$mailBody = "Hai, ".Yii::app()->user->name.". <br />Konfirmasi anda telah kami baca, kami akan melakukan proses pengendapan pada pesanan anda.";
					elseif($_POST['Donasi']['sent_email'] == 3)
						$mailBody = "Hai, ".Yii::app()->user->name.". <br />Konfirmasi anda telah kami baca, pesanan anda kami batalkan karena suatu hal, silakan kontak admin untuk lebih lanjut.";
					
					$mail = new YiiMailer();
					$mail->clearLayout();
					$mail->setTo($model->email);
					$mail->setSubject('Perihal konfirmasi order');
					$mail->setBody($mailBody);
					$mail->setLayout('universalMail');
					$mail->IsSMTP();
					if ($mail->send()) {} else {}
				}
				$this->redirect(array('index'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
			'rekeningTujuanModel'=>$rekeningTujuanModel,
			'paketDonasiModel'=>$paketDonasiModel,
			'paketDonasiSelected'=>$model->id_paket_donasi,
			'donateOptionSelected'=>$donateOptionSelected,
		));
	}

	public function actionDelete($id)
	{	
		$model = $this->loadModel($id);
		Yii::app()->controller->isUserItSelf($model->user_id);
		if($model->status_donasi != 1){
			$model->delete();
			Yii::app()->user->setFlash('success', 'Donasi berhasil dihapus');
		}
		else
			Yii::app()->user->setFlash('warning', 'Status donasi telah dikonfirmasi, tidak dapat dihapus');
		

		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	public function actionIndex()
	{
		$model=new Donasi('search');
		$indexPage = 'index';
		if(Yii::app()->user->level > 2){
			$indexPage = 'indexdonatur';
			$model=new Donasi('searchDonatur');
		}
		
		$model->unsetAttributes();
		
		$paketDonasiModels = Yii::app()->db->createCommand('SELECT id, nama_paket FROM paket_donasi')->queryAll();
		$paketDonasiModel[''] = "Pilih Paket Donasi";
		foreach($paketDonasiModels as $paketDonasiM){
			$paketDonasiMID = $paketDonasiM['id'];
			$paketDonasiModel[$paketDonasiMID] = $paketDonasiM['nama_paket'];
		}
		
		if(isset($_GET['Donasi']))
			$model->attributes=$_GET['Donasi'];

		
		$this->render($indexPage,array(
			'model'=>$model,
			'paketDonasiModel'=>$paketDonasiModel,
		));
	}
	
	public function displayDate(){
		$dateModels = Yii::app()->db->createCommand()->select('tanggal_donasi')->from('donasi')->order('id ASC')->queryAll();
		$z=1;
		$arr=array();
		$arr[''] = "Pilih Tanggal";
		foreach($dateModels as $dateModel){
			if($z==1){
				$arr[Yii::app()->dateFormatter->format("yyyy-MM",strtotime($dateModel['tanggal_donasi']))] = Yii::app()->dateFormatter->format("yyyy MMMM",strtotime($dateModel['tanggal_donasi']));
				$tanggalPrevius = Yii::app()->dateFormatter->format("yyyy-MM",strtotime($dateModel['tanggal_donasi']));
				$z++;
			}
			elseif($z>1){
				if($tanggalPrevius == Yii::app()->dateFormatter->format("yyyy-MM",strtotime($dateModel['tanggal_donasi']))){}
				else{
					$arr[Yii::app()->dateFormatter->format("yyyy-MM",strtotime($dateModel['tanggal_donasi']))] = Yii::app()->dateFormatter->format("yyyy MMMM",strtotime($dateModel['tanggal_donasi']));
				}
			}
		}
		return $arr;
	}

	public function loadModel($id)
	{
		$model=Donasi::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	public function transliterateDate($date){
		return Yii::app()->dateFormatter->format("d MMMM yyyy",strtotime($date));
	}
	
	public function getStatus($status){
		if($status == 1)
			return "Berhasil Konfirmasi";
		if($status == 2)
			return "Gagal Konfirmasi";
		else
			return "Belum di Proses";
	}
	
}
