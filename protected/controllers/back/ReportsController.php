<?php

class ReportsController extends Controller
{
	public $layout='//layouts/column2';
	
	public function filters()
	{
		return array(
			'accessControl',
			'postOnly + delete',
		);
	}
	
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('index', 'exportmonthly'),
				'users'=>array('@'),
				'expression'=>'Yii::app()->user->level < 3'
			),
			array('deny',
				'users'=>array('*'),
			),
		);
	}
	
	
	public function actionIndex(){
		$model = new Donasi('monthlyReports');
		$model->unsetAttributes();
		
		$bulanModel = array(
			'1'=>'Januari',
			'2'=>'Februari',
			'3'=>'Maret',
			'4'=>'April',
			'5'=>'Mei',
			'6'=>'Juni',
			'7'=>'Juli',
			'8'=>'Agustus',
			'9'=>'September',
			'10'=>'Oktober',
			'11'=>'November',
			'12'=>'Desember',
		);
		
        if(isset($_GET['Donasi']))
            $model->attributes=$_GET['Donasi'];
		
		$this->render('monthly', array(
			'model'=>$model,
			'bulanModel'=>$bulanModel,
		));
	}
	
	public function actionExportmonthly($bulan = NULL, $tahun = NULL ){
	
		if(empty($tahun))
			$tahun = date("Y");
			
		if(empty($bulan))
			$bulan = date("m");
		
		$reportModels = Yii::app()->db->createCommand("SELECT b.reports_date, 
		
		(SELECT COUNT(username) FROM user AS csubquery WHERE role_id > 2 AND day(reports_date) = day(csubquery.created_at) AND month(reports_date) = month(csubquery.created_at)  AND year(reports_date) = year(csubquery.created_at)) AS donatur_baru,

		(SELECT COUNT(username) FROM user AS csubquery WHERE role_id > 2) AS jumlah_donatur,

		COALESCE((SELECT COUNT(id) FROM donasi WHERE day(reports_date) = day(tanggal_donasi) AND month(reports_date) = month(tanggal_donasi) AND year(reports_date) = year(tanggal_donasi) GROUP BY b.id), 0) as jumlah_konfirmasi, 
		
		(SELECT COALESCE(SUM(dsub.donasi),0) FROM donasi AS dsub LEFT JOIN paket_donasi AS pdsub ON dsub.id_paket_donasi = pdsub.id WHERE day(dsub.tanggal_donasi) = day(b.reports_date) AND month(dsub.tanggal_donasi) = month(b.reports_date) AND year(dsub.tanggal_donasi) = year(b.reports_date) AND dsub.status_donasi = '1' ) AS total_donasi_berhasil_hari_ini, 
		
		(SELECT COALESCE(SUM(dsub.donasi),0) FROM donasi AS dsub LEFT JOIN paket_donasi AS pdsub ON dsub.id_paket_donasi = pdsub.id WHERE day(dsub.tanggal_donasi) = day(b.reports_date) AND month(dsub.tanggal_donasi) = month(b.reports_date) AND year(dsub.tanggal_donasi) = year(b.reports_date) AND dsub.status_donasi = '2' ) AS total_donasi_gagal_hari_ini, 
		
		(SELECT COALESCE(SUM(dsub.donasi),0) FROM donasi AS dsub LEFT JOIN paket_donasi AS pdsub ON dsub.id_paket_donasi = pdsub.id WHERE day(dsub.tanggal_donasi) = day(b.reports_date) AND month(dsub.tanggal_donasi) = month(b.reports_date) AND year(dsub.tanggal_donasi) = year(b.reports_date) AND dsub.status_donasi = '0' ) AS total_donasi_belum_diproses_hari_ini, 

		(SELECT total_donasi_berhasil_hari_ini + total_donasi_gagal_hari_ini + total_donasi_belum_diproses_hari_ini) AS total_donasi_hari_ini,

		(SELECT COALESCE(SUM(tsub.donasi),0) FROM donasi AS tsub RIGHT JOIN daily_reports AS b ON day(tsub.tanggal_donasi) = day(b.reports_date) AND month(tsub.tanggal_donasi) = month(b.reports_date) AND year(tsub.tanggal_donasi) = year(b.reports_date) LEFT JOIN paket_donasi AS d ON d.id = tsub.id_paket_donasi WHERE tsub.status_donasi = '1' AND month(tsub.tanggal_donasi) = {$bulan} AND year(tsub.tanggal_donasi) = {$tahun} ) AS total_donasi_berhasil_bulan_ini, 

		(SELECT COALESCE(SUM(tsub.donasi),0) FROM donasi AS tsub RIGHT JOIN daily_reports AS b ON day(tsub.tanggal_donasi) = day(b.reports_date) AND month(tsub.tanggal_donasi) = month(b.reports_date) AND year(tsub.tanggal_donasi) = year(b.reports_date) LEFT JOIN paket_donasi AS d ON d.id = tsub.id_paket_donasi WHERE tsub.status_donasi = '2' AND month(tsub.tanggal_donasi) = {$bulan} AND year(tsub.tanggal_donasi) = {$tahun} ) AS total_donasi_gagal_bulan_ini, 

		(SELECT COALESCE(SUM(tsub.donasi),0) FROM donasi AS tsub RIGHT JOIN daily_reports AS b ON day(tsub.tanggal_donasi) = day(b.reports_date) AND month(tsub.tanggal_donasi) = month(b.reports_date) AND year(tsub.tanggal_donasi) = year(b.reports_date) LEFT JOIN paket_donasi AS d ON d.id = tsub.id_paket_donasi WHERE tsub.status_donasi = '0' AND month(tsub.tanggal_donasi) = {$bulan} AND year(tsub.tanggal_donasi) = {$tahun} ) AS total_donasi_belum_proses_bulan_ini,

		(SELECT total_donasi_berhasil_bulan_ini + total_donasi_gagal_bulan_ini + total_donasi_belum_proses_bulan_ini) AS total_donasi_bulan_ini
		
		FROM donasi AS t RIGHT JOIN daily_reports AS b ON day(t.tanggal_donasi) = day(b.reports_date) AND month(t.tanggal_donasi) = month(b.reports_date) AND year(t.tanggal_donasi) = year(b.reports_date)
		
		WHERE month(reports_date) = {$bulan} AND year(reports_date) = {$tahun} GROUP BY b.id LIMIT 35")->queryAll();
		
		$reportModel = array();
		
		$reportModel[] = array(
			'Tanggal', 'Donatur Baru', 'Total Donasi Berhasil', 'Total Donasi Gagal', 'Total Donasi Belum di Proses', 'Total Semua Donasi'
		);
		
		foreach($reportModels as $reportM){
			$reportModel[] = array(
				Yii::app()->dateFormatter->format("EEEE, dd MMMM yyyy",strtotime($reportM['reports_date'])), 
				$reportM['donatur_baru'], 
				$this->convertPrice($reportM['total_donasi_berhasil_hari_ini']), 
				$this->convertPrice($reportM['total_donasi_gagal_hari_ini']), 
				$this->convertPrice($reportM['total_donasi_belum_diproses_hari_ini']), 
				$this->convertPrice($reportM['total_donasi_hari_ini']),
			);
		}
		
		$reportModel[] = array(
			'', '', '', '', '', ''
		);
		
		$reportModel[] = array(
			array(
				'value'=>'Jumlah',
				'background'=>'7BAE14',
				'font'=>array(
					'color'=>'FFFFFF'
				)
			), 
			'',
			array(
				'value'=>$this->convertPrice($reportModels[0]['total_donasi_berhasil_bulan_ini']),
				'background'=>'BCFE44',
			),
			array(
				'value'=>$this->convertPrice($reportModels[0]['total_donasi_gagal_bulan_ini']),
				'background'=>'BCFE44',
			),
			array(
				'value'=>$this->convertPrice($reportModels[0]['total_donasi_belum_proses_bulan_ini']),
				'background'=>'BCFE44',
			),
			array(
				'value'=>$this->convertPrice($reportModels[0]['total_donasi_bulan_ini']),
				'background'=>'BCFE44',
			),
		);
		
		$reportModel[] = array(
			array(
				'value'=>'Jumlah Donatur',
				'background'=>'7BAE14',
				'font'=>array(
					'color'=>'FFFFFF'
				)
			),
			array(
				'value'=>$reportModels[0]['jumlah_donatur'],
				'background'=>'BCFE44',
			),
		);
		
		$filename = "Monthly Report - ".Yii::app()->dateFormatter->format("MMMM yyyy",strtotime(date("{$tahun}-{$bulan}")));;
		$dir_path = Yii::getPathOfAlias('webroot').'/uploads/anyfile/report/';
		$filenames = $dir_path . $filename . '.xlsx';
		
		array_map('unlink', glob( "$dir_path*.xlsx"));
		
		set_time_limit(0);
		Yii::app()->yexcel->writeSheet($reportModel, $filename);
		
		if (file_exists($filenames))
			Yii::app()->getRequest()->sendFile($filename.'.xlsx', @file_get_contents($filenames));
			
		Yii::app()->user->setFlash('success', 'Berhasil melakukan ekspor');
		$this->redirect(array('index'));
	}

}
