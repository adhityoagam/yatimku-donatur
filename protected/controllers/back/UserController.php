<?php

class UserController extends Controller
{
	public $layout='//layouts/column2';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','update','delete','create'),
				'users'=>array('@'),
				'expression'=>'Yii::app()->user->level < 3'
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('settings'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionCreate()
	{
		$model=new User;
		$settingsModel = new UserSettings;

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			$settingsModel->attributes = $_POST['UserSettings'];
			
			$valid = $model->validate();
			$valid = $settingsModel->validate() && $valid;
			
			if($valid){
				$emailHash = md5("m0TH3rN4tu123".date('Y-m-d:h-i-s'));
				$passwordHash = md5("W#w1sHY0u60DSp33d");
				$PasswordOptions = [
					'salt'=>$passwordHash
				];
				
				$EmailOptions = [
					'salt'=>$emailHash
				];
				$model->key_token = password_hash($model->password, PASSWORD_DEFAULT, $EmailOptions);
				$model->password = password_hash($model->password, PASSWORD_DEFAULT, $PasswordOptions);
				$model->save(false);
				$settingsModel->user_id = $model->primaryKey;
				$settingsModel->save(false);
				Yii::app()->user->setFlash('success', 'User berhasil ditambah');
				$this->redirect(array('index'));
				
			}
		}

		$this->render('create',array(
			'model'=>$model,
			'settingsModel'=>$settingsModel,
		));
	}

	public function actionUpdate($id)
	{
		if(Yii::app()->user->id != $id && Yii::app()->user->level > 1 ){
			$this->redirect(array('user/update&id='.Yii::app()->user->id));
		}
		
		$model=$this->loadModel($id);
		$model->scenario = "update";
		$settingsModel = UserSettings::model()->findByAttributes(array('user_id'=>$id));
		$emailHash = md5("m0TH3rN4tu123".date('Y-m-d:h-i-s'));
		$passwordHash = md5("W#w1sHY0u60DSp33d");
		$PasswordOptions = [
			'salt'=>$passwordHash
		];
		
		$EmailOptions = [
			'salt'=>$emailHash
		];

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			$settingsModel->attributes = $_POST['UserSettings'];
			
			if($_POST['User']['repassword'] != "" || $_POST['User']['verifyPassword'] != "" ){
				$password= password_hash($_POST['User']['verifyPassword'], PASSWORD_DEFAULT, $PasswordOptions);
				$model->repassword = $_POST['User']['repassword'];
				$model->verifyPassword = $_POST['User']['verifyPassword'];
			}else{
				$password = $model->password;
			}
		
			$valid = $model->validate();
			$valid = $settingsModel->validate() && $valid;
		
			if($valid){
				$model->key_token = password_hash($model->password, PASSWORD_DEFAULT, $EmailOptions);
				$model->password = $password;
				$model->save(false);
				$settingsModel->save(false);
				Yii::app()->user->setFlash('success', 'User berhasil diubah');
				$this->redirect(array('index'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
			'settingsModel'=>$settingsModel,
		));
	}
	
	public function actionSettings()
	{	
		$id = Yii::app()->user->id;
		$model=$this->loadModel($id);
		$model->scenario = "update";
		$settingsModel = UserSettings::model()->findByAttributes(array('user_id'=>$id));
		$emailHash = md5("m0TH3rN4tu123".date('Y-m-d:h-i-s'));
		$passwordHash = md5("W#w1sHY0u60DSp33d");
		$PasswordOptions = [
			'salt'=>$passwordHash
		];
		
		$EmailOptions = [
			'salt'=>$emailHash
		];

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			$settingsModel->attributes = $_POST['UserSettings'];
			
			if($_POST['User']['repassword'] != "" || $_POST['User']['verifyPassword'] != "" ){
				$password= password_hash($_POST['User']['verifyPassword'], PASSWORD_DEFAULT, $PasswordOptions);
				$model->repassword = $_POST['User']['repassword'];
				$model->verifyPassword = $_POST['User']['verifyPassword'];
			}else{
				$password = $model->password;
			}
		
			$valid = $model->validate();
			$valid = $settingsModel->validate() && $valid;
		
			if($valid){
				$model->key_token = password_hash($model->password, PASSWORD_DEFAULT, $EmailOptions);
				$model->password = $password;
				$model->save(false);
				$settingsModel->save(false);
				Yii::app()->user->setFlash('success', 'User berhasil diubah');
				$this->redirect(array('index'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
			'settingsModel'=>$settingsModel,
		));
	}

	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();
		UserSettings::model()->findByAttributes(array('user_id'=>$id))->delete();
		
		Yii::app()->user->setFlash('success', 'User berhasil dihapus');
		$this->redirect(array('index'));
	}

	public function actionIndex()
	{	
		$model=new User('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['User']))
            $model->attributes=$_GET['User'];
			
		$this->render('index',array(
			'model'=>$model,
		));
	}

	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
}
