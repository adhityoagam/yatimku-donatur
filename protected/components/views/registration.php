<?php
/* @var $this AuthController */
?>
<div class="hiddenable <?php echo ($model->hasErrors())? "error":""; ?> authentic">
	<div class="">
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'registration-form',
			// Please note: When you enable ajax validation, make sure the corresponding
			// controller action is handling ajax validation correctly.
			// There is a call to performAjaxValidation() commented in generated controller code.
			// See class documentation of CActiveForm for details on this.
			'stateful'=>true,
		)); ?>
		<div>
			<table>
				<tr>
					<td>Username</td>
					<td></td>
					<td><?php echo $form->textField($model,'username', array('class'=>'form-control','id'=>'username','placeholder'=>'Username','tab-index'=>1)); ?></td>
				</tr>
				<?php echo "<tr><td colspan='3'>".$form->error($model,'username')."</td></tr>"; ?>
				<tr>
					<td>Password</td>
					<td></td>
					<td><?php echo $form->passwordField($model,'password', array('class'=>'form-control','id'=>'password','placeholder'=>'Password','tab-index'=>2)); ?></td>
				</tr><?php echo "<tr><td colspan='3'>".$form->error($model,'password')."</td></tr>"; ?>
				<tr>
					<td>Re-type Password</td>
					<td></td>
					<td><?php echo $form->passwordField($model,'verifyPassword', array('class'=>'form-control','id'=>'verifyPassword','placeholder'=>'Re-type Password','tab-index'=>3)); ?>
				</tr>
				<?php echo "<tr><td colspan='3'>".$form->error($model,'verifyPassword')."</td></tr>"; ?>
				<tr>
					<td>Phone</td>
					<td></td>
					<td><?php echo $form->textField($model,'phone', array('class'=>'form-control','id'=>'phone','placeholder'=>'Phone','tab-index'=>4)); ?></td>
				</tr>
				<?php echo "<tr><td colspan='3'>".$form->error($model,'phone')."</td></tr>"; ?>
				<tr>
					<td>Email</td>
					<td></td>
					<td><?php echo $form->emailField($model,'email', array('class'=>'form-control','id'=>'email','placeholder'=>'Email','tab-index'=>5)); ?></td>
				</tr>
				<?php echo "<tr><td colspan='3'>".$form->error($model,'email')."</td></tr>"; ?>
			</table>
			<div>
				<div><input type="submit" value="Register"/></div>
			</div>
		</div>
	</div>
	<?php $this->endWidget();?>
</div>