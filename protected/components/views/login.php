<?php
/* @var $this AuthController */
?>
<div class="hiddenable <?php echo ($model->hasErrors())? "error":""; ?> authentic">
	<div class="">
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'login-form',
			// Please note: When you enable ajax validation, make sure the corresponding
			// controller action is handling ajax validation correctly.
			// There is a call to performAjaxValidation() commented in generated controller code.
			// See class documentation of CActiveForm for details on this.
			'enableAjaxValidation'=>false,
		)); ?>
		<?php //echo $form->errorSummary($model, null,null,array('class' => 'alert alert-danger')); ?>
		<div>
			<table>
				<tr>
					<td>Username</td>
					<td></td>
					<td><?php echo $form->textField($model,'username', array('class'=>'form-control','id'=>'username','placeholder'=>'Username','tab-index'=>1)); ?></td>
					<?php echo "<tr><td colspan='3'>".$form->error($model,'username')."</td></tr>"; ?>
				</tr>
				<tr>
					<td>Password</td>
					<td></td>
					<td><?php echo $form->passwordField($model,'password', array('class'=>'form-control','id'=>'password','placeholder'=>'Password','tab-index'=>2)); ?></td>
					<?php echo "<tr><td colspan='3'>".$form->error($model,'password')."</td></tr>"; ?>
				</tr>
			</table>
			<div style="overflow:hidden;padding:0 10px;">
				<div style="float:left;">
					<a href="<?php echo Yii::app()->createUrl('auth/forgot'); ?>">Forgot Password</a>
				</div>
				<div style="float:right;">
					<input type="submit" value="Login"/>
				</div>
			</div>
			<div style="overflow:hidden;">
				<div style="float:right;padding:0 10px;margin:20px 0 0 0;">
					<label><input type="checkbox" name="Auth[rememberMe]" value="1"/> Remember Me</label>
				</div>
			</div>
		</div>
	</div>
	<?php $this->endWidget();?>
</div>