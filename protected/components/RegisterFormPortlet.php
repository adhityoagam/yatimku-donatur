<?php 
Yii::import('zii.widgets.CPortlet'); // you have to import the CPortlet first
/**
 * Portlet that is rendering login form. You can call it anywhere in your view file.
 */
class RegisterFormPortlet extends CPortlet
{
	public function init()
    {
        parent::init();
    }
	
	protected function renderContent()
    {
		$model = new RegistrationForm;
		//$this->performAjaxValidation($model);
			
		if(isset($_POST['RegistrationForm'])){
			$model->attributes = $_POST['RegistrationForm'] ;
			if($model->validate()){
				$model->activkey = md5("azzamedia".microtime().$model->password);
				$model->level = 3;
				$model->password = md5($_POST['RegistrationForm']['password']);
				$model->verifyPassword = md5($_POST['RegistrationForm']['verifyPassword']);
			}
			
			if($model->save()){
				$mail = new YiiMailer();
				//$mail->clearLayout();//if layout is already set in config
				$mail->setTo($model->email);
				//$mail->setTo(array('xxx@qq.com'=>'Scott QQ','WWSP_NOREPLY@qq.com'=>'WWSP QQ'));
				//$mail->setCC('xxx@gmail.com');
				$mail->setSubject('Konfirmasi Pendaftaran Ahaa');
				$aw = Yii::app()->getBaseUrl(true).'/auth/activation/'.$model->key_token."-".$model->primaryKey;
				$mail->setBody("Please click link below to confirm registration on The Frontage.<br /><a href='$aw'>Link</a>");
				//$mail->setAttachment(array('something.pdf'=>'Some file','something_else.pdf'=>'Another file'));
				$mail->IsSMTP();
				if ($mail->send()) {} else {}
				
				Yii::app()->user->setFlash('success', "Please activate you account via email address");
				Yii::app()->getController()->redirect(Yii::app()->createUrl('auth/landing'));
			}
			
		}
		
		$this->render('registration', array(
			'model'=>$model
		));
	}
	
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='registration-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
?>