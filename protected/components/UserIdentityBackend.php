<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentityBackend extends CUserIdentity
{
	
	const ERROR_INNACTIVE = 4;
	const ERROR_BANNED = 5;
	const ERROR_IS_NOT_ADMIN = 6;
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
		private $_id;
		private $_username;
		
		public function getName(){
			return $this->_username;
		}
		
		public function getId(){
			return $this->_id;
		}
		
		public function authenticate(){
			$passwordHash = md5("W#w1sHY0u60DSp33d");
			$PasswordOptions = [
				'salt'=>$passwordHash
			];
			$user = User::model()->findByAttributes(array('username'=>$this->username));
			if($user === null){
				$this->errorCode= self::ERROR_USERNAME_INVALID;
			}
			elseif($user->password !== password_hash($this->password, PASSWORD_DEFAULT, $PasswordOptions)){
				$this->errorCode= self::ERROR_PASSWORD_INVALID;
			}
			elseif($user->status == 0){
				$this->errorCode= self::ERROR_INNACTIVE;
			}
			elseif($user->status == -1){
				$this->errorCode= self::ERROR_BANNED;
			}
			elseif($user->role_id > 2){
				$this->errorCode= self::ERROR_PASSWORD_INVALID;
			}
			else{
				$this->_id = $user->id;
				$this->_username = $user->username;
				$this->setState('level', $user->role_id);
				$this->errorCode= self::ERROR_NONE;
			}
			return $this->errorCode;
		}
}