<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
	
	protected function breadcrumb(){
		$this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
			'tagName'=>'ul',
			'separator'=>'',
			'homeLink'=>false,
			'activeLinkTemplate'=>'<li><a href="{url}">{label}</a></li>',
			'inactiveLinkTemplate'=>'<li><span>{label}</span></li>',
			'htmlOptions'=>array(
				'class'=>'breadcrumb breadcrumb-top'
			)
		));
	}
	
	public function isUserItSelf($userid){
		if($userid != Yii::app()->user->id && Yii::app()->user->level > 2)
			throw new CHttpException(404,'Halaman yang anda cari tidak ditemukan.');
	}
	
	protected function convertPrice($price){
		return 'Rp. '.number_format($price,2,',','.');
	}
	
}