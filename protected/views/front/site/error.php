	<div class="content-header">
		<div class="header-section">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1>Error <?php echo CHtml::encode($code); ?></h1>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="block full block-alt-noborder">
		<h3 class="sub-header text-center"><strong><?php echo CHtml::encode($message); ?></strong></h3>
		<div class="row">
			<div class="col-md-12">
				<article>
				</article>
			</div>
		</div>
	</div>
<!-- END Page Content -->