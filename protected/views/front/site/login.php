<?php 
$form = $this->beginWidget('CActiveForm', array(
	'id'						=> 'form-login',
	'enableClientValidation'	=> true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
    'htmlOptions'=>array(
        'class'=>	'form-horizontal form-bordered form-control-borderless',
    ),
)); ?>
<!-- Login Form -->
	<div class="form-group">
		<div class="col-xs-12">
			<div class="input-group">
				<span class="input-group-addon"><i class="fa fa-user"></i></span>
				<?php echo $form->textField($model,'username', array('class'=>'form-control input-lg','id'=>'login-email','placeholder'=>'Username')); ?>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="col-xs-12">
			<div class="input-group">
				<span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
				<?php echo $form->passwordField($model,'password', array('class'=>'form-control input-lg','id'=>'login-password','placeholder'=>'Password')); ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-10 col-xs-push-1 mt2">
			<?php echo $form->errorSummary($model, "",null,array('class' => 'alert alert-danger listnone')); ?>
		</div>
	</div>
	<div class="form-group form-actions">
		<div class="col-xs-8 text-right">
			<button type="submit" class="btn btn-sm btn-info"><i class="fa fa-angle-right"></i> Login</button>
		</div>
	</div>
<!-- END Login Form -->
<?php $this->endWidget(); ?>