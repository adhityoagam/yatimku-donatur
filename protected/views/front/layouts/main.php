<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/plugins.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/themes.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/custom.css" />

	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/vendor/jquery-1.11.0.min.js"></script>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
</head>

<body>
	<div id="page-wrapper">
            <div id="page-container" class="sidebar-no-animations">

                <!-- Main Container -->
                <div id="main-container">
                    <header class="navbar navbar-default">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<ul class="nav navbar-nav-custom">
										<li>
											<a href="<?php echo Yii::app()->getHomeUrl(); ?>">Home</a>
										</li>
										<li>
											<a href="<?php echo Yii::app()->createUrl('donatur/konfirmasi'); ?>">Konfirmasi</a>
										</li>
										<li>
											<a href="<?php echo Yii::app()->createUrl('donatur/program'); ?>">Program Donasi</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
                    </header>
					
                    <div id="page-content">
						<?php echo $content; ?>
                    </div>

                    <footer class="clearfix">
                        <div class="pull-right">
                            Dibuat dengan <i class="fa fa-heart text-danger"></i> oleh <a href="http://www.azzamedia.com" target="_blank">AZZAMEDIA</a>
                        </div>
                        <div class="pull-left">
                            <span><?php echo date("Y"); ?></span> &copy;Yatimku Donatur
						</div>
                    </footer>
                </div>
            </div>
        </div>

	<!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
	<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/vendor/bootstrap.min.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugins.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/menu.js"></script>
</body>
</html>
