<?php
/* @var $this PageController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Beranda'=>'kelola.php',
	'Error '. CHtml::encode($code),
);
?>
<!-- Page content -->
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>Error <?php echo CHtml::encode($code); ?></h1>
		</div>
	</div>
	<?php $this->breadcrumb(); ?>
	<?php echo CHtml::encode($message); ?>
	
</div>