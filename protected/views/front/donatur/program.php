<div class="content-header">
	<div class="header-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Program Donasi</h1>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="page-content" class="pt0 mt0">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<?php foreach($model as $modelProgram): 
							$modelProgramID = $modelProgram->id; ?>
					<div class="col-sm-3">
						<div class="media-items">
							<?php if(isset($imageModel[$modelProgram->id])): ?>
								<div class="group" style="max-height:150px;overflow:hidden;">
									<img class="img-responsive" src="<?php echo Yii::app()->getBaseUrl(true); ?>/uploads/paketdonasi/<?php echo $imageModel[$modelProgram->id]; ?>" />
								</div>
								<?php endif;?>
							<h4>
								<strong><a href="<?php echo Yii::app()->createUrl('donatur/programdetail/id/'.$modelProgramID); ?>" class="nostyle themed-color-dark-fancy"><?php echo $modelProgram->nama_paket; ?></a></strong>
							</h4>
						</div>
					</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</div>