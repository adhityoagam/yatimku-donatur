<div class="content-header">
	<div class="header-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Konfirmasi Donasi</h1>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="page-content" class="pt0 mt0">
	<div class="row">
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'paket-donasi-form',
			'enableAjaxValidation'=>false,
			'htmlOptions' => array('enctype' => 'multipart/form-data'),
		)); ?>
		<div class="col-md-12">
			<div class="block full">
				<?php echo $form->errorSummary($model, null,null,array('class' => 'alert alert-danger')); ?>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<?php echo $form->labelEx($model,'nama'); ?>
							<?php echo $form->textField($model,'nama', array('class'=>'form-control','value'=>$userModel['name'])); ?>
						</div>
						
						<div class="form-group">
							<?php echo $form->labelEx($model,'email'); ?>
							<?php echo $form->textField($model,'email', array('class'=>'form-control','value'=>$userModel['email'])); ?>
						</div>
						
						<div class="form-group">
							<?php echo $form->labelEx($model,'telepon'); ?>
							<?php echo $form->textField($model,'telepon', array('class'=>'form-control','value'=>$userModel['phone'])); ?>
						</div>
						
						<div class="form-group">
							<?php echo $form->labelEx($model,'alamat'); ?>
							<?php echo $form->textArea($model,'alamat', array('class'=>'form-control')); ?>
						</div>
						
						<div id="donasiContainer" class="form-group">
							<?php echo $form->labelEx($model,'donasi'); ?>
							<?php echo $form->textField($model,'donasi',array('class'=>'form-control')); ?>
						</div>
						
						<div id="donasiContainer" class="form-group">
							<?php echo $form->labelEx($model,'rincian_donasi'); ?>
							<?php echo $form->textArea($model,'rincian_donasi',array('class'=>'form-control', 'style'=>'min-height:100px;')); ?>
						</div>
						
						<div id="paketDonasiContainer" class="form-group">
							<?php echo $form->labelEx($model,'id_paket_donasi'); ?>
							<?php echo $form->dropDownList($model,'id_paket_donasi', $paketDonasiModel,array('class'=>'form-control')); ?>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<?php echo $form->labelEx($model,'id_rekening_tujuan'); ?>
							<?php echo $form->dropDownList($model,'id_rekening_tujuan', $rekeningTujuanModel,array('class'=>'form-control')); ?>
						</div>
						
						<div class="form-group">
							<?php echo $form->labelEx($model,'nama_bank_transfer'); ?>
							<?php echo $form->textField($model,'nama_bank_transfer', array('class'=>'form-control')); ?>
						</div>
						
						<div class="form-group">
							<?php echo $form->labelEx($model,'nomor_bank_transfer'); ?>
							<?php echo $form->textField($model,'nomor_bank_transfer', array('class'=>'form-control')); ?>
						</div>
						
						<div class="form-group">
							<?php echo $form->labelEx($model,'tanggal_transfer'); ?>
							<?php echo $form->textField($model,'tanggal_transfer', array('class'=>'form-control')); ?>
						</div>
						<div>
							<?php echo CHtml::submitButton('Konfirmasi', array('class'=>'btn btn-info')); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php $this->endWidget(); ?>
	</div>
</div>
<?php setJavascript(Yii::app()->request->baseUrl."/js/custom.js"); ?>