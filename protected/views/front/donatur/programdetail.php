<div class="content-header">
	<div class="header-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Program Donasi <?php echo $model->nama_paket; ?></h1>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="page-content" class="pt0 mt0">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="media-items" style="text-align:left;">
					<div class="row">
						<div class="col-md-4">
							<?php if(isset($model->filename)): ?>
								<div class="group">
									<img class="img-responsive" src="<?php echo Yii::app()->getBaseUrl(true); ?>/uploads/paketdonasi/<?php echo $imageModel['filename']; ?>" />
								</div>
							<?php endif;?>
						</div>
						<div class="col-md-8 fs16">
							<?php echo $model->deskripsi_paket; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>