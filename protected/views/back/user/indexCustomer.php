<?php
/* @var $this PageController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Beranda'=>'kelola.php',
	'Kustomer'=>array('customer'),
	'Semua Kustomer',
);
?>
<!-- Page content -->
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>Kustomer</h1>
		</div>
	</div>
	<?php $this->breadcrumb(); ?>
	<div class="block full">
		<div class="block-title">
			<h2>Semua Kustomer</h2>
		</div>
		<?php getFlashMessage(); ?>
		<?php $this->renderPartial("_searchCustomer", array('model'=>$model,'kotaModel'=>$kotaModel)); ?>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id'=>'customer-grid',
						'pager' => array('htmlOptions'=>array('class'=>'pagination pagination-sm')),
						'itemsCssClass' => 'table table-condensed table-striped table-bordered table-hover',
						'dataProvider'=>$model->searchCustomer(),
						'columns'=>array(
							//Kolom checkbox (Untuk mass delete)
							array(
								'class'=>'CCheckBoxColumn',
								'selectableRows'=>2,
								'htmlOptions'=>array(
									'class'=>'bulk-act'
								)
							),
							array(
								'header'=>'Username',
								'name' => 'username',
								'type'=>'raw',
								'value' => '$data->username',
							),
							array(
								'header'=>'Email',
								'name'=>'email',
								'type'=>'raw',
								'value'=>'CHtml::link($data->email, "mailto:".$data->email)',
							),
							array(
								'header'=>'Dibuat Pada',
								'name'=>'created_at',
								'value'=>'User::itemAlias("DateMatching", $data->created_at)'
							),
							array(
								'header'=>'Terakhir Login',
								'name'=>'lastvisit_at',
								'value'=>'User::itemAlias("DateMatching", $data->lastvisit_at)'
							),
							array(
								'header'=>'Kota',
								'name'=>'b.city_name',
								'value'=>'$data->kota_name'
							),
							array(
								'class'=>'CButtonColumn',
								'template'=>'{update}',
								'updateButtonImageUrl'=>false,
								'updateButtonLabel' => 'Edit',
							),
							array(
								'class'=>'CButtonColumn',
								'template'=>'{delete}',
								'deleteButtonImageUrl'=>false,
								'deleteButtonLabel' => 'Hapus',
								'deleteConfirmation' => 'Apakah anda yakin?',
							),
						),
					)); ?>
				</div>
				<!-- END Table Styles Content -->
			</div>
			<div class="col-md-12">
				<div class="btn-group btn-group-sm mt1">
					<a title="" id="applyBulk" data-toggle="tooltip" class="btn btn-info mass-delete" href="javascript:void(0)" data-original-title="Hapus Halaman yang dicentang" data-id="user"><i class="fa fa-times"></i></a>
				</div>
			</div>
		</div>
	</div>
	
</div>
<?php setJavascript(Yii::app()->request->baseUrl."/js/custom.js"); ?>
<?php setJavascript(Yii::app()->request->baseUrl."/js/bulkactions.js"); ?>