<?php
/* @var $this PageController */
/* @var $model Page */

$this->breadcrumbs=array(
	'Beranda'=>'kelola.php',
	'User'=>array('index'),
	'Tambah Baru',
);
?>
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>Tambah User</h1>
		</div>
	</div>
	<?php $this->breadcrumb(); ?>
	<div class="row">
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'user-form',
			'enableAjaxValidation'=>false,
			'htmlOptions' => array('enctype' => 'multipart/form-data'),
		)); ?>
		<div class="col-md-12">
			<div class="block full">
				<?php echo $form->errorSummary($model, null,null,array('class' => 'alert alert-danger')); ?>
				<?php echo $form->errorSummary($settingsModel, null,null,array('class' => 'alert alert-danger')); ?>
				<div class="form-group">
					<?php echo $form->labelEx($model,'username'); ?>
					<?php echo $form->textField($model,'username',array('class'=>'form-control')); ?>
				</div>

				<div class="form-group">
					<?php echo $form->labelEx($model,'password'); ?>
					<?php echo $form->passwordField($model,'password',array('class'=>'form-control')); ?>
				</div>

				<div class="form-group">
					<?php echo $form->labelEx($model,'repassword'); ?>
					<?php echo $form->passwordField($model,'repassword',array('class'=>'form-control')); ?>
				</div>

				<div class="form-group">
					<?php echo $form->labelEx($model,'email'); ?>
					<?php echo $form->emailField($model,'email',array('class'=>'form-control')); ?>
				</div>

				<div class="form-group">
					<?php echo $form->labelEx($model,'status'); ?>
					<?php echo $form->dropDownList($model,'status', array('1'=>'Active','0'=>'Unactive','3'=>'Banned')); ?>
				</div>

				<div class="form-group">
					<?php echo $form->labelEx($model,'role_id'); ?>
					<?php echo $form->dropDownList($model,'role_id', User::itemAlias('AdminStatus')); ?>
				</div>
			</div>
			<div class="block full">
				<div class="form-group">
					<?php echo $form->labelEx($settingsModel,'nama_lengkap'); ?>
					<?php echo $form->textField($settingsModel,'nama_lengkap',array('class'=>'form-control')); ?>
				</div>

				<div class="form-group">
					<?php echo $form->labelEx($settingsModel,'no_telp'); ?>
					<?php echo $form->textField($settingsModel,'no_telp', array('class'=>'form-control')); ?>
				</div>
				
				<div>
					<?php echo CHtml::submitButton('Tambah', array('class'=>'btn btn-info')); ?>
				</div>
			</div>
		</div>
	<?php $this->endWidget(); ?>
	</div>
	<?php setJavascript(Yii::app()->baseUrl."/js/custom.js"); ?>
</div>
