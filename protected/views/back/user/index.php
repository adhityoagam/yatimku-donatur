<?php
/* @var $this PageController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Beranda'=>'kelola.php',
	'Manajemen User'=>array('index'),
	'Semua User',
);
?>
<!-- Page content -->
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>User</h1>
		</div>
	</div>
	<?php $this->breadcrumb(); ?>
	<div class="block full">
		<div class="block-title">
			<h2>Semua User</h2>
		</div>
		<?php getFlashMessage(); ?>
		<?php $this->renderPartial("_search", array('model'=>$model)); ?>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id'=>'user-grid',
						'pager' => array('htmlOptions'=>array('class'=>'pagination pagination-sm')),
						'itemsCssClass' => 'table table-condensed table-striped table-bordered table-hover',
						'dataProvider'=>$model->search(),
						'columns'=>array(
							//Kolom checkbox (Untuk mass delete)
							array(
								'class'=>'CCheckBoxColumn',
								'selectableRows'=>2,
								'htmlOptions'=>array(
									'class'=>'bulk-act'
								)
							),
							array(
								'header'=>'Username',
								'name' => 'username',
								'type'=>'raw',
								'value' => '$data->username',
							),
							array(
								'header'=>'Email',
								'name'=>'email',
								'type'=>'raw',
								'value'=>'CHtml::link($data->email, "mailto:".$data->email)',
							),
							array(
								'header'=>'Dibuat Pada',
								'name'=>'created_at',
								'value'=>'User::itemAlias("DateMatching", $data->created_at)'
							),
							array(
								'header'=>'Terakhir Login',
								'name'=>'lastvisit_at',
								'value'=>'User::itemAlias("DateMatching", $data->lastvisit_at)'
							),
							array(
								'header'=>'Level User',
								'name'=>'role_id',
								'value'=>'User::itemAlias("UserLevel", $data->role_id)',
							),
							array(
								'header'=>'Status',
								'name'=>'status',
								'value'=>'User::itemAlias("UserStats", $data->status)',
							),
							array(
								'class'=>'CButtonColumn',
								'template'=>'{update}',
								'updateButtonImageUrl'=>false,
								'updateButtonLabel' => 'Edit',
							),
							array(
								'class'=>'CButtonColumn',
								'template'=>'{delete}',
								'deleteButtonImageUrl'=>false,
								'deleteButtonLabel' => 'Hapus',
								'deleteConfirmation' => 'Apakah anda yakin?',
							),
						),
					)); ?>
				</div>
				<!-- END Table Styles Content -->
			</div>
			<div class="col-md-12">
				<div class="btn-group btn-group-sm mt1">
					<a title="" id="applyBulk" data-toggle="tooltip" class="btn btn-info mass-delete" href="javascript:void(0)" data-original-title="Hapus Halaman yang dicentang" data-url="<?php echo Yii::app()->createUrl("user/delactajax");?>"><i class="fa fa-times"></i></a>
				</div>
			</div>
		</div>
	</div>
	
</div>
<?php setJavascript(Yii::app()->request->baseUrl."/js/custom.js"); ?>
<?php setJavascript(Yii::app()->request->baseUrl."/js/bulkactions.js"); ?>