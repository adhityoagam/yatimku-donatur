<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-3 pt2">
					<a href="<?php echo Yii::app()->createUrl("user/tambah"); ?>" class="btn btn-info">Tambah User</a>
				</div>
				<div class="col-md-9">
					<div class="row">
						<?php $form=$this->beginWidget('CActiveForm', array(
							'action'=>Yii::app()->createUrl($this->route),
							'method'=>'get', 
						)); ?>
							<div class="col-md-4">
								<strong>Filter Level User</strong>
								<?php echo $form->dropDownList($model,'role_id',$model->itemAlias('AdminStatus'), array('class'=>'form-control','submit'=>'')); ?>
							</div>
							<div class="col-md-4">
								<strong>Filter Status User</strong>
								<?php echo $form->dropDownList($model,'status',$model->itemAlias('UserStatus'), array('class'=>'form-control','submit'=>'')); ?>
							</div>
							<div class="col-md-4">
								<strong>Cari</strong>
								<?php echo $form->textField($model,'username',array('class'=>'form-control','submit'=>'','placeholder'=>'Ketik Username Disini')); ?>
							</div>
							<div class="hidden">
								<input type="submit">
							</div>
						<?php $this->endWidget(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>