<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Beranda'=>'kelola.php',
	'User'=>array('index'),
	'Edit',
);
?>
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>Edit User</h1>
		</div>
	</div>
	<?php $this->breadcrumb(); ?>
	<div class="row">
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'user-form',
			'enableAjaxValidation'=>false,
			'htmlOptions' => array('enctype' => 'multipart/form-data'),
		)); ?>
		<div class="col-md-12">
			<div class="block full">
				<?php echo $form->errorSummary(array($model, $settingsModel), null,null,array('class' => 'alert alert-danger')); ?>
				<div class="form-group">
					<?php echo $form->labelEx($model,'verifyPassword'); ?>
					<?php echo $form->passwordField($model,'verifyPassword',array('class'=>'form-control')); ?>
				</div>
				
				<div class="form-group">
					<?php echo $form->labelEx($model,'repassword'); ?>
					<?php echo $form->passwordField($model,'repassword',array('class'=>'form-control')); ?>
				</div>

				<div class="form-group">
					<?php echo $form->labelEx($model,'email'); ?>
					<?php echo $form->emailField($model,'email',array('class'=>'form-control')); ?>
				</div>

				<div class="form-group">
					<?php echo $form->labelEx($model,'status'); ?>
					<?php echo $form->dropDownList($model,'status', array('1'=>'Active','0'=>'Unactive','3'=>'Banned')); ?>
				</div>
				
				<div class="form-group">
					<?php echo $form->labelEx($model,'role_id'); ?>
					<?php echo $form->dropDownList($model,'role_id', User::itemAlias('AdminStatus')); ?>
				</div>

			</div>
			<div class="block full">
				<div class="form-group">
					<?php echo $form->labelEx($settingsModel,'nama_lengkap'); ?>
					<?php echo $form->textField($settingsModel,'nama_lengkap',array('class'=>'form-control')); ?>
				</div>

				<div class="form-group">
					<?php echo $form->labelEx($settingsModel,'no_telp'); ?>
					<?php echo $form->textField($settingsModel,'no_telp', array('class'=>'form-control')); ?>
				</div>
				
				<div>
					<?php echo CHtml::submitButton('Simpan', array('class'=>'btn btn-info')); ?>
				</div>
			</div>
		</div>
	<?php $this->endWidget(); ?>
	</div>
</div>
<?php setJavascript(Yii::app()->baseUrl."/js/custom.js"); ?>