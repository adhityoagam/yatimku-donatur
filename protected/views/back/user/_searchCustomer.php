<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-3 pt2">
					<a href="<?php echo Yii::app()->createUrl("user/exportcustomer"); ?>" class="btn btn-info">Expor Kustomer</a>
				</div>
				<div class="col-md-9">
					<div class="row">
						<?php $form=$this->beginWidget('CActiveForm', array(
							'action'=>Yii::app()->createUrl($this->route),
							'method'=>'get', 
						)); ?>
							<div class="col-md-3 col-md-push-6">
								<strong>Filter Kota</strong>
								<?php echo $form->dropDownList($model,'kota_id', $kotaModel, array('class'=>'form-control','submit'=>'')); ?>
							</div>
							<div class="col-md-3 col-md-push-6">
								<strong>Cari</strong>
								<?php echo $form->textField($model,'username',array('class'=>'form-control','submit'=>'','placeholder'=>'Ketik Username Disini')); ?>
							</div>
							<div class="hidden">
								<input type="submit">
							</div>
						<?php $this->endWidget(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>