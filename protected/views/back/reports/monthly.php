<?php
$this->breadcrumbs=array(
	'Beranda'=>'kelola.php',
	'Monthly Reports'=>array('index'),
	'All',
);
?>
<!-- Page content -->
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>Monthly Reports</h1>
		</div>
	</div>
	<?php $this->breadcrumb(); ?>
	<div class="block full">
		<div class="block-title">
			<h2>All Monthly Reports</h2>
		</div>
		<?php getFlashMessage(); ?>
		<?php $this->renderPartial("_searchMonthly", array('model'=>$model, 'bulanModel'=>$bulanModel)); ?>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
				<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id'=>'user-grid',
						'pager' => array(
							'htmlOptions'=>array(
								'class'=>'pagination pagination-sm mt1 mb0'
							),
							'header'=>''
						),
						'htmlOptions'=>array(
							'class'=>'p0 grid-view',
							'autocomplete'=>'off'
						),
						'emptyText'=>'Data tidak ditemukan',
						'itemsCssClass'=>'table table-condensed table-striped table-bordered table-hover mb0 mt1',
						'dataProvider'=>$model->monthlyReports(),
						'ajaxUpdate'=>false,
						'columns'=>array(
							array(
								'header'=>'Tanggal',
								'name' => 'b.reports_date',
								'value' => 'Yii::app()->dateFormatter->format("EEEE, dd MMMM yyyy",strtotime($data->reports_date))',
								'htmlOptions'=>array(
									'width'=>200
								),
							),
							array(
								'header'=>'Donatur Baru',
								'name' => 'donatur_baru',
								'value' => '$data->donatur_baru',
								'htmlOptions'=>array(
									'width'=>110
								),
							),
							array(
								'header'=>'Jumlah Konfirmasi',
								'name' => 'jumlah_konfirmasi',
								'value' => '$data->jumlah_konfirmasi',
								'htmlOptions'=>array(
									'width'=>150
								),
							),
							array(
								'header'=>'Total Donasi',
								'name' => 'total_donasi_hari_ini',
								'value' => 'Yii::app()->controller->convertPrice($data->total_donasi_hari_ini)',
							),
						),
					)); ?>
				</div>
				<!-- END Table Styles Content -->
			</div>
		</div>
	</div>
	
</div>
<?php setJavascript(Yii::app()->request->baseUrl."/js/custom.js"); ?>
<?php setJavascript(Yii::app()->request->baseUrl."/js/bulkactions.js"); ?>