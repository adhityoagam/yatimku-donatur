<div class="row mb1">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-3 pt2">
				<a href="<?php echo Yii::app()->createUrl("reports/exportcustomer"); ?>" class="btn btn-info">Export</a>
			</div>
			<div class="col-md-9">
				<div class="row">
					<?php $form=$this->beginWidget('CActiveForm', array(
						'action'=>Yii::app()->createUrl($this->route),
						'method'=>'get',
						'htmlOptions'=>array(
							'autocomplete'=>'off'
						)
					)); ?>
						<div class="col-md-3 col-md-push-9">
							<strong>Cari</strong>
							<?php echo $form->textField($model,'nama_lengkap',array('class'=>'form-control','placeholder'=>'Nama Kustomer','submit'=>'','autocomplete'=>'off')); ?>
						</div>
					<?php $this->endWidget(); ?>
				</div>
			</div>
		</div>
	</div>
</div>