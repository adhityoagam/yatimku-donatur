<?php
/* @var $this PageController */
/* @var $model Page */
/* @var $form CActiveForm */
?>
	<div class="row mb1">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-3 pt2">
					<a href="<?php echo Yii::app()->createUrl("page/tambah"); ?>" class="btn btn-info">Tambah Baru</a>
				</div>
				<div class="col-md-9">
					<div class="row">
						<?php $form=$this->beginWidget('CActiveForm', array(
							'action'=>Yii::app()->createUrl($this->route),
							'method'=>'get',
							'htmlOptions'=>array(
								'autocomplete'=>'off'
							)
						)); ?>
							<div class="col-md-4">
								<strong>Filter Tanggal</strong>
								<?php 
									echo $form->dropDownList($model, 'created_at', $this->displayDate(), array('empty' => 'Pilih Tanggal', 'class'=>'form-control','submit'=>'','placeholder'=>''));
								?>
							</div>
							<div class="col-md-4">
								<strong>Filter Status</strong>
								<?php echo $form->dropDownList($model, 'page_status', array(''=>'Status','1'=>'Published ('.Page::searchStatus(1).')', '2'=>'Draft ('.Page::searchStatus(2).')', '3'=>'Trash ('.Page::searchStatus(3).')'), array('class'=>'form-control','submit'=>'')); ?>
							</div>
							<div class="col-md-4">
								<strong>Cari</strong>
								<?php echo $form->textField($model,'page_title',array('class'=>'form-control','placeholder'=>'Ketik Judul Disini','submit'=>'','autocomplete'=>'off')); ?>
							</div>
						<?php $this->endWidget(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>