<?php
/* @var $this PageController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Beranda'=>'kelola.php',
	'Laporan Produk'=>array('index'),
	'Semua',
);
?>
<!-- Page content -->
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>Laporan Produk</h1>
		</div>
	</div>
	<?php $this->breadcrumb(); ?>
	<div class="block full">
		<div class="block-title">
			<h2>Semua Laporan Produk</h2>
		</div>
		<?php getFlashMessage(); ?>
		<?php $this->renderPartial("_searchProduct", array('model'=>$model)); ?>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
				<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id'=>'user-grid',
						'pager' => array(
							'htmlOptions'=>array(
								'class'=>'pagination pagination-sm mt1 mb0'
							),
							'header'=>''
						),
						'htmlOptions'=>array(
							'class'=>'p0 grid-view',
							'autocomplete'=>'off'
						),
						'emptyText'=>'Data tidak ditemukan',
						'itemsCssClass'=>'table table-condensed table-striped table-bordered table-hover mb0 mt1',
						'dataProvider'=>$model->productReports(),
						'ajaxUpdate'=>false,
						'columns'=>array(
							array(
								'header'=>'Kode Produk',
								'name' => 'product_code',
								'value' => '$data->product_code',
							),
							array(
								'header'=>'Nama Produk',
								'name' => 'product_title',
								'value' => '$data->product_title',
							),
							array(
								'header'=>'Ordered',
								'name' => 'product_ordered_quantity',
								'value' => '$data->product_ordered_quantity',
							),
							array(
								'header'=>'Sold',
								'name' => 'product_sold_quantity',
								'value' => '$data->product_sold_quantity',
							),
							array(
								'header'=>'Stock',
								'name' => 'a.stok',
								'value' => '$data->stok',
							),
							array(
								'header'=>'Viewed',
								'name' => 'product_view_count',
								'value' => '$data->product_view_count',
							),
						),
					)); ?>
				</div>
				<!-- END Table Styles Content -->
			</div>
		</div>
	</div>
	
</div>
<?php setJavascript(Yii::app()->request->baseUrl."/js/custom.js"); ?>
<?php setJavascript(Yii::app()->request->baseUrl."/js/bulkactions.js"); ?>