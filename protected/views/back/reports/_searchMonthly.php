<div class="row mb1">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-3 pt2">
				<?php 	$bulan = date("m"); if(isset($_GET['Donasi']['bulan']) && $_GET['Donasi']['bulan'] != "") $bulan = $_GET['Donasi']['bulan'];
						$tahun = date("Y"); if(isset($_GET['Donasi']['tahun']) && $_GET['Donasi']['tahun'] != "") $tahun = $_GET['Donasi']['tahun']; ?>
				<a href="<?php echo Yii::app()->createUrl("reports/exportmonthly&bulan={$bulan}&tahun={$tahun}"); ?>" class="btn btn-info">Export</a>
			</div>
			<div class="col-md-9">
				<div class="row">
					<?php $form=$this->beginWidget('CActiveForm', array(
						'action'=>Yii::app()->createUrl($this->route),
						'method'=>'get',
						'htmlOptions'=>array(
							'autocomplete'=>'off'
						)
					)); ?>
						<div class="col-md-3 col-md-push-6">
							<strong>Filter Bulan</strong>
							
							<?php echo CHtml::dropDownList("Donasi[bulan]", $bulan, $bulanModel,array('class'=>'form-control','placeholder'=>'Nama Produk','submit'=>'','autocomplete'=>'off')); ?>
						</div>
						<div class="col-md-3 col-md-push-6">
							<strong>Filter Tahun</strong>
							
							<?php echo $form->textField($model,'tahun', array('class'=>'form-control','placeholder'=>'Tahun','submit'=>'','autocomplete'=>'off','value'=>$tahun)); ?>
						</div>
					<?php $this->endWidget(); ?>
				</div>
			</div>
		</div>
	</div>
</div>