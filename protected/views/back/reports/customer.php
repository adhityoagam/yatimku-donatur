<?php
$this->breadcrumbs=array(
	'Beranda'=>'kelola.php',
	'Laporan Kustomer'=>array('index'),
	'Semua',
);
?>
<!-- Page content -->
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>Laporan Kustomer</h1>
		</div>
	</div>
	<?php $this->breadcrumb(); ?>
	<div class="block full">
		<div class="block-title">
			<h2>Semua Laporan Kustomer</h2>
		</div>
		<?php getFlashMessage(); ?>
		<?php $this->renderPartial("_searchCustomer", array('model'=>$model)); ?>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
				<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id'=>'user-grid',
						'pager' => array(
							'htmlOptions'=>array(
								'class'=>'pagination pagination-sm mt1 mb0'
							),
							'header'=>''
						),
						'htmlOptions'=>array(
							'class'=>'p0 grid-view',
							'autocomplete'=>'off'
						),
						'emptyText'=>'Data tidak ditemukan',
						'itemsCssClass'=>'table table-condensed table-striped table-bordered table-hover mb0 mt1',
						'dataProvider'=>$model->customerReports(),
						'ajaxUpdate'=>false,
						'columns'=>array(
							array(
								'header'=>'Nama Kustomer',
								'name' => 'nama_lengkap',
								'value' => '$data->nama_lengkap',
							),
							array(
								'header'=>'Email Kustomer',
								'name' => 'email',
								'value' => '$data->email',
							),
							array(
								'header'=>'Jumlah Order',
								'name' => 'order_count',
								'value' => '$data->order_count',
							),
							array(
								'header'=>'Total Order',
								'name' => 'buy_order',
								'value' => 'Yii::app()->controller->convertPrice($data->buy_order)',
							),
							array(
								'header'=>'Jumlah Transaksi',
								'name' => 'transaction_count',
								'value' => '$data->transaction_count',
							),
							array(
								'header'=>'Total Transaksi',
								'name' => 'transaction_order',
								'value' => 'Yii::app()->controller->convertPrice($data->total_transaction)',
							),
						),
					)); ?>
				</div>
				<!-- END Table Styles Content -->
			</div>
		</div>
	</div>
	
</div>
<?php setJavascript(Yii::app()->request->baseUrl."/js/custom.js"); ?>
<?php setJavascript(Yii::app()->request->baseUrl."/js/bulkactions.js"); ?>