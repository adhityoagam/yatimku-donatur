
<!-- Login Block -->
<div class="block push-bit">
	<!-- Register Form -->
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'auth-form',
			'enableAjaxValidation'=>false,
			'htmlOptions' => array('enctype' => 'multipart/form-data','class'=>'form-horizontal','autocomplete'=>'off'),
		)); ?>
		<?php echo $form->errorSummary(array($model, $settingsModel), null,null,array('class' => 'alert alert-danger')); ?>
		<div class="form-group">
			<div class="col-xs-12">
				<div class="input-group">
					<span class="input-group-addon"><i class="gi gi-user"></i></span>
					<?php echo $form->textField($model,'username',array('class'=>'form-control input-lg', 'autocomplete'=>'off', 'placeholder'=>$model->attributeLabels()['username'])); ?>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-xs-12">
				<div class="input-group">
					<span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
					<?php echo $form->passwordField($model,'password',array('class'=>'form-control input-lg','placeholder'=>$model->attributeLabels()['password'])); ?>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-xs-12">
				<div class="input-group">
					<span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
					<?php echo $form->passwordField($model,'repassword',array('class'=>'form-control input-lg','placeholder'=>$model->attributeLabels()['repassword'])); ?>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-xs-12">
				<div class="input-group">
					<span class="input-group-addon"><i class="gi gi-envelope"></i></span>
					<?php echo $form->emailField($model,'email',array('class'=>'form-control input-lg', 'autocomplete'=>'off', 'placeholder'=>$model->attributeLabels()['email'])); ?>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-xs-12">
				<div class="input-group">
					<span class="input-group-addon"><i class="gi gi-phone_alt"></i></span>
					<?php echo $form->textField($settingsModel,'no_telp',array('class'=>'form-control input-lg', 'autocomplete'=>'off', 'placeholder'=>$settingsModel->attributeLabels()['no_telp'])); ?>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-xs-12">
				<div class="input-group">
					<span class="input-group-addon"><i class="gi gi-user"></i></span>
					<?php echo $form->textField($settingsModel,'nama_lengkap',array('class'=>'form-control input-lg', 'autocomplete'=>'off', 'placeholder'=>$settingsModel->attributeLabels()['nama_lengkap'])); ?>
				</div>
			</div>
		</div>
				
		<div class="form-group">
			<div class="center-block text-center">
				<?php echo CHtml::submitButton('Daftar', array('class'=>'btn btn-info')); ?>
			</div>
		</div>
		
		<div class="form-group">
			<div class="col-xs-12 text-center">
			<strong>Sudah punya akun? </strong><a href="<?php echo Yii::app()->createUrl('auth/login'); ?>" id="link-register" class="ml1"><strong>Login</strong></a>
			</div>
		</div>
	<?php $this->endWidget(); ?>
	
	<!-- END Register Form -->
</div>
<!-- END Login Block -->
<?php setJavascript(Yii::app()->baseUrl."/js/custom.js"); ?>