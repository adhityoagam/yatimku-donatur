<?php
$this->breadcrumbs=array(
	'Beranda'=>'kelola.php',
	'Rekening'=>array('index'),
	'Lihat',
);
?>
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>Lihat Rekening</h1>
		</div>
	</div>
	<?php $this->breadcrumb(); ?>
	<div class="row">
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'rekening-form',
			'enableAjaxValidation'=>false,
			'htmlOptions' => array('enctype' => 'multipart/form-data'),
		)); ?>
		<div class="col-md-12">
			<div class="block full">
				
				<div class="form-group">
					<?php echo $form->labelEx($model,'bank'); ?>
					<?php echo $form->textField($model,'bank',array('class'=>'form-control', 'readonly'=>'readonly')); ?>
				</div>
				
				<div class="form-group">
					<?php echo $form->labelEx($model,'nama_rekening'); ?>
					<?php echo $form->textField($model,'nama_rekening',array('class'=>'form-control', 'readonly'=>'readonly')); ?>
				</div>
				
				<div class="form-group">
					<?php echo $form->labelEx($model,'nomor_rekening'); ?>
					<?php echo $form->textField($model,'nomor_rekening',array('class'=>'form-control', 'readonly'=>'readonly')); ?>
				</div>
				
				<div class="form-group">
					<?php echo $form->labelEx($model,'keterangan'); ?>
					<?php echo $form->textarea($model,'keterangan',array('class'=>'form-control', 'readonly'=>'readonly')); ?>
				</div>
			</div>
		</div>
	<?php $this->endWidget(); ?>
	</div>
</div>