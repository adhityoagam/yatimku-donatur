<?php
$this->breadcrumbs=array(
	'Beranda'=>'kelola.php',
	'Rekening'=>array('index'),
	'Tambah',
);
?>
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>Tambah Rekening</h1>
		</div>
	</div>
	<?php $this->breadcrumb(); ?>
	<div class="row">
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'rekening-form',
			'enableAjaxValidation'=>false,
			'htmlOptions' => array('enctype' => 'multipart/form-data'),
		)); ?>
		<div class="col-md-12">
			<div class="block full">
				<?php echo $form->errorSummary($model, null,null,array('class' => 'alert alert-danger')); ?>
				
				<div class="form-group">
					<?php echo $form->labelEx($model,'bank'); ?>
					<?php echo $form->textField($model,'bank',array('class'=>'form-control')); ?>
				</div>
				
				<div class="form-group">
					<?php echo $form->labelEx($model,'nama_rekening'); ?>
					<?php echo $form->textField($model,'nama_rekening',array('class'=>'form-control')); ?>
				</div>
				
				<div class="form-group">
					<?php echo $form->labelEx($model,'nomor_rekening'); ?>
					<?php echo $form->textField($model,'nomor_rekening',array('class'=>'form-control')); ?>
				</div>
				
				<div class="form-group">
					<?php echo $form->labelEx($model,'keterangan'); ?>
					<?php echo $form->textarea($model,'keterangan',array('class'=>'form-control')); ?>
				</div>
				
				<div>
					<?php echo CHtml::submitButton('Tambah', array('class'=>'btn btn-info')); ?>
				</div>
			</div>
		</div>
	<?php $this->endWidget(); ?>
	</div>
</div>