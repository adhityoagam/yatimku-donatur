<?php
/* @var $this PageController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Beranda'=>'kelola.php',
	'Rekening'=>array('index'),
	'Semua Rekening',
);
?>
<!-- Page content -->
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>Rekening</h1>
		</div>
	</div>
	<?php $this->breadcrumb(); ?>
	<div class="block full">
		<div class="block-title">
			<h2>Semua Rekening</h2>
		</div>
		<?php getFlashMessage(); ?>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id'=>'rekening-grid',
						'pager' => array('htmlOptions'=>array('class'=>'pagination pagination-sm')),
						'itemsCssClass' => 'table table-condensed table-striped table-bordered table-hover',
						'dataProvider'=>$model->search(),
						'columns'=>array(
							array(
								'header'=>'Bank',
								'name' => 'bank',
								'value' => '$data->bank',
							),
							array(
								'header'=>'Nama',
								'name' => 'nama_rekening',
								'value' => '$data->nama_rekening',
							),
							array(
								'header'=>'Nomor Rekening',
								'name' => 'nomor_rekening',
								'value' => '$data->nomor_rekening',
							),
							array(
								'class'=>'CButtonColumn',
								'template'=>'{view}',
								'viewButtonImageUrl'=>false,
								'viewButtonLabel' => 'Lihat',
							),
						),
					)); ?>
				</div>
				<!-- END Table Styles Content -->
			</div>
		</div>
	</div>
	
</div>