<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/plugins.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/themes.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/custom.css" />

	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/vendor/jquery-1.11.0.min.js"></script>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
</head>

<body>
	<div id="page-container" class="sidebar-partial sidebar-visible-lg sidebar-no-animations header-fixed-top">
		<!-- Main Sidebar -->
		<div id="sidebar">
			<!-- Wrapper for scrolling functionality -->
			<div class="sidebar-scroll">
				<!-- Sidebar Content -->
				<div class="sidebar-content">
					<!-- Brand -->
					<a href="index.html" class="sidebar-brand">
						Yatimku Donatur
					</a>
					<!-- END Brand -->

					<!-- Sidebar Navigation -->
					<?php $this->renderPartial('../layouts/menu'); ?>
					<!-- END Sidebar Navigation -->

				</div>
				<!-- END Sidebar Content -->
			</div>
			<!-- END Wrapper for scrolling functionality -->
		</div>
		<!-- END Main Sidebar -->

		<!-- Main Container -->
		<div id="main-container">
			<!-- Header -->
			<header class="navbar header-fixed-top navbar-inverse navbar-fixed-top">
				<!-- Right Header Navigation -->
				<ul class="nav navbar-nav-custom pull-right">
					<!-- END Alternative Sidebar Toggle Button -->

					<!-- User Dropdown -->
					<li class="dropdown">
						<a href="javascript:void(0)" class="dropdown-toggle fs16" data-toggle="dropdown">
							Hai, <?php echo Yii::app()->user->name; ?> <i class="fs19 fa fa-angle-down"></i>
						</a>
						<ul class="dropdown-menu dropdown-custom dropdown-menu-right">
							<li>
								<a href="<?php echo Yii::app()->createUrl('kelola/logout'); ?>"><i class="fa fa-ban fa-fw pull-right"></i> Keluar</a>
							</li>
						</ul>
					</li>
					<!-- END User Dropdown -->
				</ul>
				<!-- END Right Header Navigation -->
			</header>
			<!-- END Header -->

			<?php echo $content; ?>

			<!-- END Footer -->
		</div>
		<!-- END Main Container -->
	</div>
	<!-- END Page Container -->

	<!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
	<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/vendor/bootstrap.min.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugins.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/menu.js"></script>
</body>
</html>
