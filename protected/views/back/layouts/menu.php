<ul class="sidebar-nav">
    <li>
        <a href="<?php echo Yii::app()->homeUrl; ?>"><i class="fa fa-dashboard sidebar-nav-icon"></i>Dashboard</a>
    </li>
    <li>
        <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-right sidebar-nav-indicator"></i><i class="fa fa-university sidebar-nav-icon"></i>Donasi</a>
        <ul>
            <li>
                <a href="<?php echo Yii::app()->createUrl("donasi"); ?>">Donasi</a>
            </li>
            <li>
                <a href="<?php echo Yii::app()->createUrl("paketdonasi"); ?>">Paket Donasi</a>
            </li>
            <li>
                <a href="<?php echo Yii::app()->createUrl("reports"); ?>">Reports</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-right sidebar-nav-indicator"></i><i class="fa fa-credit-card sidebar-nav-icon"></i>Rekening</a>
        <ul>
            <li>
                <a href="<?php echo Yii::app()->createUrl("rekeningpembayaran"); ?>">Semua Rekening</a>
            </li>
			<?php if(Yii::app()->user->level < 2): ?>
            <li>
                <a href="<?php echo Yii::app()->createUrl("rekeningpembayaran/create"); ?>">Tambah Rekening</a>
            </li>
			<?php endif;?>
        </ul>
    </li>
    <li>
        <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-right sidebar-nav-indicator"></i><i class="gi gi-user sidebar-nav-icon"></i>User</a>
        <ul>
			<?php if(Yii::app()->user->level < 2): ?>
            <li>
                <a href="<?php echo Yii::app()->createUrl("user"); ?>">Semua Donatur</a>
            </li>
            <li>
                <a href="<?php echo Yii::app()->createUrl("user/create"); ?>">Tambah Donatur</a>
            </li>
			<?php endif;?>
            <li>
                <a href="<?php echo Yii::app()->createUrl("user/settings"); ?>">Pengaturan</a>
            </li>
        </ul>
    </li>
</ul>