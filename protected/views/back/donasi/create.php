<?php
$this->breadcrumbs=array(
	'Beranda'=>'kelola.php',
	'Konfirmasi Donasi'=>array('index'),
	'Konfirmasi Donasi',
);
?>
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>Konfirmasi Donasi</h1>
		</div>
	</div>
	<?php $this->breadcrumb(); ?>
	<div class="row">
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'paket-donasi-form',
			'enableAjaxValidation'=>false,
			'htmlOptions' => array('enctype' => 'multipart/form-data'),
		)); ?>
		<div class="col-md-12">
			<div class="block full">
				<?php echo $form->errorSummary($model, null,null,array('class' => 'alert alert-danger')); ?>
				<div class="form-group">
					<?php echo $form->labelEx($model,'rekening_asal'); ?>
					<?php echo $form->textField($model,'rekening_asal', array('class'=>'form-control')); ?>
				</div>
				
				<div class="form-group">
					<?php echo $form->labelEx($model,'id_rekening_tujuan'); ?>
					<?php echo $form->dropDownList($model,'id_rekening_tujuan', $rekeningTujuanModel,array('class'=>'form-control')); ?>
				</div>
				
				<div class="form-group">
					<?php echo $form->labelEx($model,'donateOption'); ?>
					<?php echo $form->dropdownList($model,'donateOption', array('1'=>'Donasi Normal', '2'=>'Paket Donasi'),array('class'=>'form-control')); ?>
				</div>
				
				<div id="donasiContainer" class="form-group">
					<?php echo $form->labelEx($model,'donasi'); ?>
					<?php echo $form->textField($model,'donasi',array('class'=>'form-control')); ?>
				</div>
				
				<div id="paketDonasiContainer" class="form-group">
					<?php echo $form->labelEx($model,'id_paket_donasi'); ?>
					<?php echo $form->dropDownList($model,'id_paket_donasi', $paketDonasiModel,array('class'=>'form-control')); ?>
				</div>

				<div>
					<?php echo CHtml::submitButton('Konfirmasi', array('class'=>'btn btn-info')); ?>
				</div>
			</div>
		</div>
	<?php $this->endWidget(); ?>
	</div>
</div>
	<?php setJavascript(Yii::app()->baseUrl."/js/custom.js"); ?>