<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<?php $form=$this->beginWidget('CActiveForm', array(
						'action'=>Yii::app()->createUrl($this->route),
						'method'=>'get',
						'htmlOptions'=>array(
							'autocomplete'=>'off'
						)
					)); ?>
						<div class="col-md-3 col-md-push-3">
							<strong>Paket Donasi</strong>
							<?php echo $form->dropDownList($model, 'id_paket_donasi', $paketDonasiModel, array('class'=>'form-control select-chosen','submit'=>'')); ?>
						</div>
						<div class="col-md-3 col-md-push-3">
							<strong>Filter Tanggal</strong>
							<?php echo $form->dropDownList($model, 'tanggal_donasi',  $this->displayDate(), array('class'=>'form-control select-chosen','submit'=>'')); ?>
						</div>
						<div class="col-md-3 col-md-push-3">
							<strong>Nama</strong>
							<?php echo $form->textField($model, 'nama_lengkap', array('class'=>'form-control','submit'=>'')); ?>
						</div>
					<?php $this->endWidget(); ?>
				</div>
			</div>
		</div>
	</div>
</div>