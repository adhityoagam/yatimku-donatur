<?php
$this->breadcrumbs=array(
	'Beranda'=>'kelola.php',
	'Donasi'=>array('index'),
	'Semua Donasi',
);
?>
<!-- Page content -->
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>Donasi</h1>
		</div>
	</div>
	<?php $this->breadcrumb(); ?>
	<div class="block full">
		<div class="block-title">
			<h2>Semua Donasi</h2>
		</div>
		<?php getFlashMessage(); ?>
		<?php $this->renderPartial("_search", array('model'=>$model, 'paketDonasiModel'=>$paketDonasiModel)); ?>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id'=>'donasi-grid',
						'pager' => array('htmlOptions'=>array('class'=>'pagination pagination-sm')),
						'itemsCssClass' => 'table table-condensed table-striped table-bordered table-hover',
						'dataProvider'=>$model->search(),
						'columns'=>array(
							array(
								'header'=>'Nama Donatur',
								'name' => 'nama',
								'value' => '$data->nama',
							),
							array(
								'header'=>'Jumlah Donasi',
								'name' => 't.donasi',
								'value' => 'Yii::app()->controller->convertPrice($data->donasi)',
							),
							array(
								'header'=>'Paket Donasi',
								'name' => 'jumlah_paket_donasi',
								'value' => '$data->jumlah_paket_donasi',
							),
							array(
								'header'=>'Status Donasi',
								'name' => 'status_donasi',
								'value' => 'Yii::app()->controller->getStatus($data->status_donasi)',
							),
							array(
								'header'=>'Tanggal Donasi',
								'name' => 'tanggal_donasi',
								'value' => 'Yii::app()->controller->transliterateDate($data->tanggal_donasi)',
							),
							array(
								'class'=>'CButtonColumn',
								'template'=>'{update}',
								'updateButtonImageUrl'=>false,
								'updateButtonLabel' => 'Edit',
							),
							array(
								'class'=>'CButtonColumn',
								'template'=>'{delete}',
								'deleteButtonImageUrl'=>false,
								'deleteButtonLabel' => 'Hapus',
								'deleteConfirmation' => 'Apakah anda yakin?',
							),
						),
					)); ?>
				</div>
				<!-- END Table Styles Content -->
			</div>
		</div>
	</div>
	
</div>
<?php setJavascript(Yii::app()->request->baseUrl."/js/custom.js"); ?>
<?php setJavascript(Yii::app()->request->baseUrl."/js/bulkactions.js"); ?>