<?php
$this->breadcrumbs=array(
	'Beranda'=>'kelola.php',
	'Paket Donasi'=>array('index'),
	'Semua Paket Donasi',
);
?>
<!-- Page content -->
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>Paket Donasi</h1>
		</div>
	</div>
	<?php $this->breadcrumb(); ?>
	<div class="block full">
		<div class="block-title">
			<h2>Semua Paket Donasi</h2>
		</div>
		<?php getFlashMessage(); ?>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id'=>'paket-donasi-grid',
						'pager' => array('htmlOptions'=>array('class'=>'pagination pagination-sm')),
						'itemsCssClass' => 'table table-condensed table-striped table-bordered table-hover',
						'dataProvider'=>$model->search(),
						'columns'=>array(
							array(
								'header'=>'Nama',
								'name' => 'nama_paket',
								'value' => '$data->nama_paket',
							),
							array(
								'header'=>'Donasi',
								'name'=>'donasi',
								'value'=>'Yii::app()->controller->convertPrice($data->donasi)',
							),
							array(
								'class'=>'CButtonColumn',
								'template'=>'{view}',
								'viewButtonImageUrl'=>false,
								'viewButtonLabel' => 'Lihat',
							),
						),
					)); ?>
				</div>
				<!-- END Table Styles Content -->
			</div>
		</div>
	</div>
	
</div>