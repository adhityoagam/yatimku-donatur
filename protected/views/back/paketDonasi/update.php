<?php
/* @var $this PageController */
/* @var $model Page */

$this->breadcrumbs=array(
	'Beranda'=>'kelola.php',
	'Paket Donasi'=>array('index'),
	'Edit Paket Donasi',
);
?>
<div id="page-content">
	<div class="content-header">
		<div class="header-section">
			<h1>Edit Paket Donasi</h1>
		</div>
	</div>
	<?php $this->breadcrumb(); ?>
	<div class="row">
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'paket-donasi-form',
			'enableAjaxValidation'=>false,
			'htmlOptions' => array(
				'enctype' => 'multipart/form-data',
				'autocomplete'=>'off',
				'class'	=>'parent-form',
				'data-post-type'=>'paketdonasi',	
			),
		)); ?>
		<div class="col-md-12">
			<div class="block full">
				<?php echo $form->errorSummary($model, null,null,array('class' => 'alert alert-danger')); ?>
				<div class="form-group">
					<?php echo $form->labelEx($model,'nama_paket'); ?>
					<?php echo $form->textField($model,'nama_paket',array('class'=>'form-control')); ?>
				</div>
				<div class="form-group">
					<?php echo $form->labelEx($model,'donasi'); ?>
					<?php echo $form->textField($model,'donasi',array('class'=>'form-control')); ?>
				</div>
					<div class="form-group">
					<?php
						if($model->isNewRecord)
							$url = Yii::app()->createUrl("paketdonasi/upload");
						else
							$url = Yii::app()->createUrl("paketdonasi/upload&id=".Yii::app()->request->getParam('id'));
						
						$this->widget('xupload.XUpload', array(
												'url' => $url,
												'model' => $photos,
												'attribute' => 'downloadfile',
												'multiple' => false,
												'htmlOptions' => array('id'=>'paket-donasi-form'),
												'options' => array(
													'maxFileSize' => 2000000,
													'acceptFileTypes' => "js:/(\.|\/)(jpe?g|png|gif|bmp|pdf|docx|doc|txt|zip|rar|mp4|mkv|flv|ppt|pptx|xls|xlsx)$/i",
													'submit' => "js:function (e, data) {
														var inputs = data.context.find(':input');
														data.formData = inputs.serializeArray();
														return true;
													}",
												),
						));
					?>
				</div>

				<div class="form-group">
					<?php echo $form->labelEx($model,'deskripsi_paket'); ?>
					<?php echo $form->textArea($model,'deskripsi_paket',array('class'=>'form-control yii-redactor')); ?>
					<?php
						Yii::import('ext.imperavi-redactor-widget.ImperaviRedactorWidget');
						$this->widget('ImperaviRedactorWidget', array(
							'selector' => '.yii-redactor',
							'options' => array(
								'lang' => 'id',
								'minHeight'=>200,
							),
							'plugins' => array(
								'fullscreen' => array(
									'js' => array('fullscreen.js'),
								),
							),
						));
					?>
				</div>

				<div>
					<?php echo CHtml::submitButton('Simpan', array('class'=>'btn btn-info')); ?>
				</div>
			</div>
		</div>
	<?php $this->endWidget(); ?>
	</div>
</div>
<div class="modal fade" id="newsForm" tabindex="-1" role="dialog" aria-labelledby="newsForm" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Edit </h4>
			</div>
			<div class="modal-body">
				<div>
					<div id="judul-caption">
						Title
					</div>
					<div>
						<input class="form-control caption-title" name="news_caption_title" />
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary save-caption" data-modul="paketdonasi">Save</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
	<?php setJavascript(Yii::app()->baseUrl."/js/delAjaxImage.js"); ?>