<?php
class Donasi extends CActiveRecord
{
	
	public $username, $jumlah_paket_donasi, $full_rekening, $nama_lengkap;
	public $donateOption, $sent_email;
	public $reports_date, $donatur_baru, $total_donasi_hari_ini, $jumlah_konfirmasi, $bulan, $tahun;// MonthlyReports
	public function tableName()
	{
		return 'donasi';
	}

	public function rules()
	{
		return array(
			array('donasi, id_rekening_tujuan, nama, email, nama_bank_transfer, nomor_bank_transfer', 'required'),
			array('nama_bank_transfer, id_paket_donasi, alamat, telepon, user_id, status_donasi, tanggal_transfer', 'default'),
			array('user_id, donasi, id_paket_donasi', 'numerical', 'integerOnly'=>true),
			array('tanggal_donasi','default',
				  'value'=>new CDbExpression('NOW()'),
				  'on'=>'insert'),
			array('id, user_id, donasi, id_paket_donasi, tanggal_donasi', 'safe', 'on'=>'search'),
			array('bulan, tahun', 'safe', 'on'=>'monthlyReports'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'donasi' => 'Nominal Donasi',
			'id_paket_donasi' => 'Program Donasi',
			'id_rekening_tujuan' => 'Bank Tujuan',
			'donateOption' => 'Opsi Donasi',
			'nama_bank_transfer' => 'Bank Asal Transfer',
			'nomor_bank_transfer' => 'Nomor Rekening Bank Asal',
			'tanggal_donasi' => 'Tanggal Donasi',
			'sent_email' => 'Kirim Email',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;
		
		$criteria->select = "t.id, status_donasi, nama, t.donasi, id_paket_donasi, tanggal_donasi, CONCAT(b.nama_paket, ' - Rp. ', FORMAT(b.donasi, 2, 'ID_id')) AS jumlah_paket_donasi, c.bank, c.nama_rekening, c.nomor_rekening, CONCAT(c.bank, ' ', c.nama_rekening, ' - ', c.nomor_rekening) AS full_rekening";
		$criteria->join = "LEFT JOIN paket_donasi AS b ON b.id = t.id_paket_donasi INNER JOIN rekening_pembayaran AS c ON c.id = t.id_rekening_tujuan";
		
		$criteria->compare('id',$this->id);
		$criteria->compare('donasi',$this->donasi);
		$criteria->compare('id_paket_donasi',$this->id_paket_donasi);
		$criteria->compare('tanggal_donasi',$this->tanggal_donasi,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'t.id ASC',
				'attributes'=>array(
					't.donasi',
					'nama',
					'id_paket_donasi',
					'tanggal_donasi',
					'status_donasi',
					'jumlah_paket_donasi'=>array(
						'asc'=>'jumlah_paket_donasi ASC',
						'desc'=>'jumlah_paket_donasi DESC',
					),
				),
			),
		));
	}
	
	public function monthlyReports(){
	
		$criteria=new CDbCriteria;
		
		$tahun = date("Y");
		$month = date("m");
		
		if(isset($this->tahun) && $this->tahun != "")
			$tahun = $this->tahun;
			
		if(isset($this->bulan)){
			$month = $this->bulan;
		}
		
		$criteria->select = "
		b.reports_date, 
		
		(SELECT COUNT(username) FROM user AS csubquery WHERE role_id > 2 AND day(reports_date) = day(csubquery.created_at) AND month(reports_date) = month(csubquery.created_at)  AND year(reports_date) = year(csubquery.created_at)) AS donatur_baru,

		(SELECT COUNT(username) FROM user AS csubquery WHERE role_id > 2) AS jumlah_donatur,

		COALESCE((SELECT COUNT(id) FROM donasi WHERE day(reports_date) = day(tanggal_donasi) AND month(reports_date) = month(tanggal_donasi) AND year(reports_date) = year(tanggal_donasi) GROUP BY b.id), 0) as jumlah_konfirmasi, 
		
		(SELECT COALESCE(SUM(dsub.donasi),0) FROM donasi AS dsub LEFT JOIN paket_donasi AS pdsub ON dsub.id_paket_donasi = pdsub.id WHERE day(dsub.tanggal_donasi) = day(b.reports_date) AND month(dsub.tanggal_donasi) = month(b.reports_date) AND year(dsub.tanggal_donasi) = year(b.reports_date) AND dsub.status_donasi = '1' ) AS total_donasi_berhasil_hari_ini, 
		
		(SELECT COALESCE(SUM(dsub.donasi),0) FROM donasi AS dsub LEFT JOIN paket_donasi AS pdsub ON dsub.id_paket_donasi = pdsub.id WHERE day(dsub.tanggal_donasi) = day(b.reports_date) AND month(dsub.tanggal_donasi) = month(b.reports_date) AND year(dsub.tanggal_donasi) = year(b.reports_date) AND dsub.status_donasi = '2' ) AS total_donasi_gagal_hari_ini, 
		
		(SELECT COALESCE(SUM(dsub.donasi),0) FROM donasi AS dsub LEFT JOIN paket_donasi AS pdsub ON dsub.id_paket_donasi = pdsub.id WHERE day(dsub.tanggal_donasi) = day(b.reports_date) AND month(dsub.tanggal_donasi) = month(b.reports_date) AND year(dsub.tanggal_donasi) = year(b.reports_date) AND dsub.status_donasi = '0' ) AS total_donasi_belum_diproses_hari_ini, 

		(SELECT total_donasi_berhasil_hari_ini + total_donasi_gagal_hari_ini + total_donasi_belum_diproses_hari_ini) AS total_donasi_hari_ini,

		(SELECT COALESCE(SUM(tsub.donasi),0) FROM donasi AS tsub RIGHT JOIN daily_reports AS b ON day(tsub.tanggal_donasi) = day(b.reports_date) AND month(tsub.tanggal_donasi) = month(b.reports_date) AND year(tsub.tanggal_donasi) = year(b.reports_date) LEFT JOIN paket_donasi AS d ON d.id = tsub.id_paket_donasi WHERE tsub.status_donasi = '1') AS total_donasi_berhasil_bulan_ini, 

		(SELECT COALESCE(SUM(tsub.donasi),0) FROM donasi AS tsub RIGHT JOIN daily_reports AS b ON day(tsub.tanggal_donasi) = day(b.reports_date) AND month(tsub.tanggal_donasi) = month(b.reports_date) AND year(tsub.tanggal_donasi) = year(b.reports_date) LEFT JOIN paket_donasi AS d ON d.id = tsub.id_paket_donasi WHERE tsub.status_donasi = '2') AS total_donasi_gagal_bulan_ini, 

		(SELECT COALESCE(SUM(tsub.donasi),0) FROM donasi AS tsub RIGHT JOIN daily_reports AS b ON day(tsub.tanggal_donasi) = day(b.reports_date) AND month(tsub.tanggal_donasi) = month(b.reports_date) AND year(tsub.tanggal_donasi) = year(b.reports_date) LEFT JOIN paket_donasi AS d ON d.id = tsub.id_paket_donasi WHERE tsub.status_donasi = '0') AS total_donasi_belum_proses_bulan_ini,

		(SELECT total_donasi_berhasil_bulan_ini + total_donasi_gagal_bulan_ini + total_donasi_belum_proses_bulan_ini) AS total_donasi_bulan_ini";
		
		$criteria->join = "RIGHT JOIN daily_reports AS b ON day(t.tanggal_donasi) = day(b.reports_date) AND month(t.tanggal_donasi) = month(b.reports_date) AND year(t.tanggal_donasi) = year(b.reports_date)";
		
		$criteria->condition = "month(reports_date) = {$month} AND year(reports_date) = {$tahun}";
		
		$criteria->group = "b.id";

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'reports_date ASC',
				'attributes'=>array(
					'b.reports_date',
					'donatur_baru'=>array(
						'asc'=>'donatur_baru ASC',
						'desc'=>'donatur_baru DESC',
					),
					'jumlah_konfirmasi'=>array(
						'asc'=>'jumlah_konfirmasi ASC',
						'desc'=>'jumlah_konfirmasi DESC',
					),
					'total_donasi_hari_ini'=>array(
						'asc'=>'total_donasi_hari_ini ASC',
						'desc'=>'total_donasi_hari_ini DESC',
					),
				),
			),
			'pagination'=>array(
				'pageSize'=>35,
			),
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
