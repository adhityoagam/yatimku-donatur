<?php
class PaketDonasi extends CActiveRecord
{
	public function tableName()
	{
		return 'paket_donasi';
	}

	public function rules()
	{
		return array(
			array('nama_paket, donasi', 'required'),
			array('deskripsi_paket', 'default'),
			array('donasi', 'numerical', 'integerOnly'=>true),
			array('nama_paket', 'length', 'max'=>50),
			array('id, nama_paket, donasi, deskripsi_paket', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama_paket' => 'Nama Paket',
			'donasi' => 'Besar Donasi',
			'deskripsi_paket' => 'Deskripsi Paket',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama_paket',$this->nama_paket,true);
		$criteria->compare('donasi',$this->donasi);
		$criteria->compare('deskripsi_paket',$this->deskripsi_paket,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
