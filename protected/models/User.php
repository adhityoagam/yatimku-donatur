<?php
class User extends CActiveRecord
{	
	public $repassword, $verifyPassword, $email_forgot, $kota_name, $kota_id;
	// Form Customer Reports
	public $nama_lengkap, $order_count, $buy_order, $transaction_count, $total_transaction, $transaction_order;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, password, status, role_id, email', 'required', 'on'=>'insert'),
			array('password, status, role_id, email', 'required', 'on'=>'update'),
			array('username', 'unique', 'message' => "Username sudah terpakai."),
			array('email', 'unique', 'message' => "Email sudah terpakai."),
			array('username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u','message' => "Ada huruf yang tidak diperbolehkan (A-z dan 0-9)."),
			array('username', 'length', 'max'=>20, 'min' => 3,'message' => "Username salah (panjang karaktek minimal 3 maksimal 20)."),
			array('status, role_id', 'numerical', 'integerOnly'=>true),
			array('username, email', 'length', 'max'=>45),
			array('email', 'email'),
			array('password, key_token', 'length', 'max'=>100),
			array('created_at, lastvisit_at', 'safe'),
			array('repassword', 'compare', 'compareAttribute'=>'password', 'message'=>"Password tidak cocok.", 'on'=>'insert'),
			array('repassword', 'compare', 'compareAttribute'=>'verifyPassword', 'message' => "Password tidak cocok.", 'on'=>'update'),
			array('email_forgot', 'email', 'on'=>'forgot'),
			array('email_forgot', 'required', 'on'=>'forgot'),
			array('created_at','default',
				  'value'=>new CDbExpression('NOW()'),
				  'setOnEmpty'=>false,'on'=>'insert'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('username, status, role_id', 'safe', 'on'=>'search'),
			array('username, status, kota_id', 'safe', 'on'=>'searchCustomer'),
			array('nama_lengkap', 'safe', 'on'=>'costumerReports'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Username',
			'email' => 'Email',
			'password' => 'Password',
			'key' => 'Key',
			'status' => 'Status',
			'role_id' => 'Level User',
			'created_at' => 'Created At',
			'lastvisit_at' => 'Terakhir Login',
			'repassword' => 'Ulangi Password',
			'email_forgot' => 'Email',
			'verifyPassword' => 'Password',
		);
	}
	
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		$criteria->compare('username',$this->username,true);
		$criteria->compare('role_id',$this->role_id);
		$criteria->compare('status',$this->status);
		$criteria->addCondition('role_id < 3');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'created_at DESC',
			)
		));
	}
	
	public function searchCustomer()
	{
		$criteria=new CDbCriteria;
		$criteria->select = "t.id, t.username, t.email, t.created_at, t.lastvisit_at, b.city_name AS kota_name";
		$criteria->join = "INNER JOIN user_settings AS a ON a.user_id = t.id LEFT JOIN region_city AS b ON a.kota = b.city_id";
		
		$criteria->compare('b.city_id',$this->kota_id);
		
		$criteria->compare('username',$this->username,true);
		$criteria->addCondition('role_id > 2');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'created_at DESC',
				'attributes'=>array(
					'username', 'email', 'created_at', 'lastvisit_at', 'b.city_name'
				)
			)
		));
	}
	
	
	public function customerReports()
	{
		$criteria = new CDbCriteria();
		$criteria->select = "a.nama_lengkap, t.email, (SELECT COUNT(id) FROM checkout_order WHERE t.id = user_id) AS order_count, (SELECT SUM(total_pembelian) FROM checkout_order WHERE t.id = user_id) AS buy_order, (SELECT COUNT(id) FROM checkout_order WHERE t.id = user_id AND status = '1') AS transaction_count, (SELECT SUM(total_pembelian) FROM checkout_order WHERE t.id = user_id AND status = '1') AS total_transaction";
		$criteria->join = "INNER JOIN user_settings AS a ON t.id = a.user_id LEFT JOIN checkout_order AS b ON t.id = b.user_id";
		$criteria->condition = "role_id > 2";
		$criteria->group = "t.id";
		
		$criteria->compare('a.nama_lengkap', $this->nama_lengkap, true);
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'t.id ASC',
				'attributes'=>array(
					'nama_lengkap',
					'email',
					'order_count' => array(
						'asc'=>'COUNT(b.id) ASC',
						'desc'=>'COUNT(b.id) DESC'
					),
					'buy_order' => array(
						'asc'=>'SUM(total_pembelian) ASC',
						'desc'=>'SUM(total_pembelian) DESC'
					),
					'transaction_count' => array(
						'asc'=>'transaction_count ASC',
						'desc'=>'transaction_count DESC'
					),
					'transaction_order' => array(
						'asc'=>'total_transaction ASC',
						'desc'=>'total_transaction DESC'
					)
				)
			)
		));
	}
	
	public static function itemAlias($type, $code=NULL) {
		$return = "";
		if($type == 'AdminStatus'){
			$returnT = Yii::app()->db->createCommand("SELECT id, name FROM user_role")->queryAll();
			$return = array();
			$return[''] = 'Semua Level';
			foreach($returnT as $key => $returns){
				$return[$returns['id']] = $returns['name'];
			}
		}elseif($type == 'UserStatus'){
			$return = array(
				''=>'Semua User',
				'1'=>'Aktif',
				'0'=>'Belum Aktif',
				'2'=>'Blokir',
			);
		}elseif($type == 'UserLevel'){
			$returnT = Yii::app()->db->createCommand("SELECT name FROM user_role WHERE id = {$code}")->queryRow();
			$return = $returnT['name'];
		}elseif($type == 'UserStats'){
			if($code == 0){
				$return = "Belum Aktif";
			}elseif($code == 1){
				$return = "Aktif";
			}elseif($code == 2){
				$return = "Blokir";
			}
		}elseif($type == 'DateMatching'){
			if(empty($code)){
				$return = "Belum Pernah Login";
			}else{
				$return = Yii::app()->dateFormatter->format("dd MMMM yyyy",strtotime($code));
			}
		}
		
		return $return;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
