<?php
class UserSettings extends CActiveRecord
{
	public function tableName()
	{
		return 'user_settings';
	}

	public function rules()
	{
		return array(
			array('nama_lengkap, no_telp', 'required'),
			array('nama_lengkap', 'length', 'max'=>100),
			array('no_telp', 'length', 'max'=>15),
			array('id, user_id, nama_lengkap, no_telp', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'nama_lengkap' => 'Nama Lengkap',
			'no_telp' => 'No Telp',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('nama_lengkap',$this->nama_lengkap,true);
		$criteria->compare('no_telp',$this->no_telp,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
