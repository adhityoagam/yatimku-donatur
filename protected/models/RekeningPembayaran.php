<?php
class RekeningPembayaran extends CActiveRecord
{
	public function tableName()
	{
		return 'rekening_pembayaran';
	}

	public function rules()
	{
		return array(
			array('bank, nama_rekening, nomor_rekening', 'required'),
			array('keterangan', 'default'),
			array('nama_rekening', 'length', 'max'=>50),
			array('id, nama_rekening, nomor_rekening, keterangan', 'safe', 'on'=>'search'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama_rekening' => 'Nama Rekening',
			'nomor_rekening' => 'Nomor Rekening',
			'keterangan' => 'Keterangan',
		);
	}

	
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama_rekening',$this->nama_rekening,true);
		$criteria->compare('nomor_rekening',$this->nomor_rekening,true);
		$criteria->compare('keterangan',$this->keterangan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
