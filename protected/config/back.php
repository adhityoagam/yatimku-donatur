<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return CMap::mergeArray(
    require(dirname(__FILE__).'/main.php'),
    array(
        'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
		'name'=>'Donatur Yatimku',
		'defaultController'=>'kelola',
		'timeZone' => 'Asia/Jakarta',
		'language' => 'ID',

		// preloading 'log' component
		'preload'=>array('log'),

		// autoloading model and component classes
		'import'=>array(
			'application.models.*',
			'application.components.*',
			'ext.YiiMail.YiiMailer',
		),
		'aliases'=>array(
			'xupload' => 'ext.xupload'
		),
		'modules'=>array(
			// uncomment the following to enable the Gii tool
			'gii'=>array(
				'class'=>'system.gii.GiiModule',
				'password'=>'azzamedia',
				// If removed, Gii defaults to localhost only. Edit carefully to taste.
				'ipFilters'=>array('127.0.0.1','::1'),
			),	
		),
		

		// application components
		'components'=>array(		
			// uncomment the following to enable URLs in path-format
			/*'urlManager'=>array(
				'urlFormat'=>'path',
				'showScriptName'=>false,
				'rules'=>array(
					'<controller:\w+>/<id:\d+>'=>'<controller>/view',
					'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
					'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
				),
			),*/
			
			// 'db'=>array(
				// 'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
			// ),
			
			// uncomment the following to use a MySQL database
			'db'=>array(
				'connectionString' => 'mysql:host=localhost;dbname=yatimku_donatur',
				'emulatePrepare' => true,
				'username' => 'root',
				'password' => '',
				'charset' => 'utf8',
			),
			'yexcel' => array(
				'class' => 'ext.yexcel.Yexcel'
			),
			'user'=>array(
				'class'=>'application.components.EWebUser',
				'allowAutoLogin'=>true,
				'loginUrl'=>array('kelola/login'),
			),
			'errorHandler'=>array(
				// use 'site/error' action to display errors
				'errorAction'=>'kelola/error',
			),
			'log'=>array(
				'class'=>'CLogRouter',
				'routes'=>array(
					array(
						'class'=>'CFileLogRoute',
						'levels'=>'error, warning',
					),
					// uncomment the following to show log messages on web pages
					/*
					array(
						'class'=>'CWebLogRoute',
					),
					*/
				),
			),
			'clientScript'=>array(
				'class' => 'CClientScript',
						'scriptMap' => array(
								'jquery.js'=>false,
								'jquery.min.js'=>false,
						),
				'coreScriptPosition' => CClientScript::POS_END,
			),
		),
		

		// application-level parameters that can be accessed
		// using Yii::app()->params['paramName']
		'params'=>array(
			// this is used in contact page
			'adminEmail'=>'webmaster@example.com',
		),
    )
);