<?php 
	
	function setJavascript($url){
		Yii::app()->clientScript->registerScriptFile($url,CClientScript::POS_END);
	}
	
	function setCss($url){
		Yii::app()->clientScript->registerCssFile($url);
	}
	
	function getFlashMessage(){
		$messages = Yii::app()->user->getFlashes();
		foreach($messages as $key => $message):
			echo "<div class='alert alert-dismissable alert-$key'>";
			echo "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
				echo $message;
			echo "</div>";
		endforeach;
	}

?>