$(document).ready(function(){
	$('#applyBulk').click(function(){
		z = $(this).attr('data-url');
		var chkArray = [],
			attributes = [];
		/* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
		$(".bulk-act input[type=checkbox]:checked").each(function() {
			chkArray.push($(this).val());
		});
		if(chkArray.length == 0){
			alert("Tidak ada data yang dicentang");
		}else{
			$.ajax({
				type: 'POST',
				url: z,
				dataType: 'json',
				data: {ajaxCall: chkArray, ajaxCallAttribute: attributes},
				success:	function(data){
					if(data.urel != ""){
						window.location = data.urel;
					}
					else{
						window.location = window.location;
					}
				},
				error:  function(data){
					window.location = window.location;
				},
			});
		}
	});
});