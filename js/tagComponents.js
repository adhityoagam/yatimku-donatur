$(function() {
    //Populate all select boxes with from select#source
    var opts=$("#source").html(), opts2="<option></option>"+opts;
    $("select.populate").each(function() { var e=$(this); e.html(e.hasClass("placeholder")?opts2:opts); });
	
	var lastResults = [];
	$("#selectTag").select2({
		tags:[],
		multiple: true,
		placeholder: "Tag",
		tokenSeparators: [","],
		minimumInputLength: 2,
		ajax: {
			multiple: true,
			url: "kelola.php?r=post/tagajax",
			dataType: "json",
			type: "POST",
			data: function (term) {
				return {
					q: term
				};
			},
			results: function(data){
				if(data){
					return { results: data };
				}
				else{
					return { results: data };
				}
			}
		},
		createSearchChoice: function (term) {
			var text = term + (lastResults.some(function(r) { return r.text == term }) ? "" : " (baru)");
			return { id: term, text: text };
		},
		initSelection: function(element, callback) {
			var data = [];
			$(element.val().split(",")).each(function () {
				data.push({id: this, text: this});
			});
			callback(data);
		},
	});
	
	$("#selectTag").on("select2-loaded", function(e){
		$('#select2-drop li').each(function(){
			if($(this).children('div.select2-result-label').text() == ""){
				$(this).remove();
			}
		});
	});
	
	$("#Coupon_product_id").select2({
		tags:[],
		multiple: true,
		placeholder: "Produk",
		tokenSeparators: [","],
		minimumInputLength: 2,
		ajax: {
			multiple: true,
			url: "kelola.php?r=coupon/productspecific",
			dataType: "json",
			type: "POST",
			data: function (term) {
				return {
					q: term
				};
			},
			results: function(data){
				if(data){
					return { results: data };
				}
				else{
					return { results: data };
				}
			}
		},
		initSelection: function(element, callback) {
			var data = [];
			$(element.val().split(",")).each(function () {
				data.push({id: this, text: this});
			});
			callback(data);
		},
	});
	
	$("#Coupon_product_id").on("select2-loaded", function(e){
		$('#select2-drop li').each(function(){
			if($(this).children('div.select2-result-label').text() == ""){
				$(this).remove();
			}
		});
	});
	
	$("#Coupon_category_id").select2({
		tags:[],
		multiple: true,
		placeholder: "Produk",
		tokenSeparators: [","],
		minimumInputLength: 2,
		ajax: {
			multiple: true,
			url: "kelola.php?r=coupon/categoryspecific",
			dataType: "json",
			type: "POST",
			data: function (term) {
				return {
					q: term
				};
			},
			results: function(data){
				if(data){
					return { results: data };
				}
				else{
					return { results: data };
				}
			}
		},
		initSelection: function(element, callback) {
			var data = [];
			$(element.val().split(",")).each(function () {
				data.push({id: this, text: this});
			});
			callback(data);
		},
	});
	
	$("#Coupon_category_id").on("select2-loaded", function(e){
		$('#select2-drop li').each(function(){
			if($(this).children('div.select2-result-label').text() == ""){
				$(this).remove();
			}
		});
	});
});