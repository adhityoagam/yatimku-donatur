jQuery(document).ready(function(){
	var mediaList,
		selectedArr,
		detailedArr,
		mediaPostType = $('form.parent-form').attr('data-post-type'),
		mediaMaxFiles = 0,
		mediaDataCalledArr,
		mediaOptions = {},
		_featuredArr,
		isFeatured = false,
		videoType = ["mp4","mkv"],
		musicType = ["mp3"],
		pdfType = ["pdf"],
		compressedType = ["zip","rar","gzip"],
		wordType = ["doc","docx"],
		powerpointType = ["ppt","pptx"],
		imageType = ["jpg","jpeg","png","gif"];
	
	var button_insert_post_text = $('#insert-post'),
		insert_media_text_h1 = $('#insertMedia h1');
		
//API//
	//selectedArr: Array yang berisikan item yang di select pada media;
	//detailedArr: array yang berisikan item yang sedang aktif pada detail
		
//Uncaught Error: Attempting to use a disconnected port object
//jquery.each will slow your website down dude, try to use plain javascript for loop!
//http://stackoverflow.com/questions/3943494/how-to-loop-through-array-in-jquery
	
	$.fn.isAfter = function(sel){
        return this.prevAll().filter(sel).length !== 0;
    }
	
	function showMedia(filterType, dataCalledArr){
		$('#mediaHolderTruly').show();
		loadMedia(filterType, dataCalledArr);
	}
	
	$('.load-featured-gallery').click(function(){
		optionar = {
			'textFieldId': '#gallery-textfield',
			'divImageDiffer': '#gallery-container-container',
		}
		dataCalledArr = {
			'type': 'image',
			'date': '',
			'name': ''
		}
		showFeatured(5,optionar,'filter',dataCalledArr);
		isFeatured = true;
	});
	
	function showFeatured(maxFile, options, filterType, dataCalledArr){
		if(!options.insert_media_text_h1_text){options.insert_media_text_h1_text = 'Set Featured'}
		
		if(!options.button_insert_post_text){options.button_insert_post_text = 'Insert Featured'}
		
		if(!options.textFieldId){options.textFieldId = '#media-featured'}
		
		if(!options.divImageDiffer){options.divImageDiffer = '.featured-container'}
		
		insert_media_text_h1.text(options.insert_media_text_h1_text);
		button_insert_post_text.text(options.button_insert_post_text);
		$('#mediaHolderTruly').show().addClass('featured');
		loadFeatured(maxFile, options, filterType, dataCalledArr);
		
	}
	
	function insertFeatured(){
		var featuredOnId = $(mediaOptions.textFieldId),
			featuredHolder = $(mediaOptions.divImageDiffer),
			stringify = "";
		
		if(featuredOnId.val() == ","){
			featuredOnId.val("");
		}
		
		_featuredArr = featuredOnId.val();
		if(_featuredArr[_featuredArr.length-1] === "," ){
			_featuredArr = _featuredArr.slice(0,-1).split(',');
		}else if(featuredOnId.val() == "" || featuredOnId.val() == ","){
			_featuredArr = [];
		}else{
			_featuredArr = _featuredArr.split(',');
		}
		
		$(selectedArr).each(function(i,e){
			if($.inArray(e.id+'', _featuredArr) === -1){
				var mediaitem = '<div class="mt2"><i class="fa fa-file fa-5x"></i></div>';
				if($.inArray(e.type,pdfType) !== -1){
					mediaitem = '<div class="mt2"><i class="fa fa-file-pdf-o fa-5x"></i></div>';
				}
				else if($.inArray(e.type,imageType) !== -1){
					mediaitem = '<div class="centered"><img src="'+$(this)[0].path+$(this)[0].filename+'" /></div>';
				}
				else if($.inArray(e.type,musicType) !== -1){
					mediaitem = '<div class="mt2"><i class="fa fa-file-sound-o fa-5x"></i></div>';
				}
				else if($.inArray(e.type,videoType) !== -1){
					mediaitem = '<div class="mt2"><i class="fa fa-file-movie-o fa-5x"></i></div>';
				}
				else if($.inArray(e.type,compressedType) !== -1){
					mediaitem = '<div class="mt2"><i class="fa fa-file-archive-o fa-5x"></i></div>';
				}
				else if($.inArray(e.type,wordType) !== -1){
					mediaitem = '<div class="mt2"><i class="fa fa-file-word-o fa-5x"></i></div>';
				}
				else if($.inArray(e.type,powerpointType) !== -1){
					mediaitem = '<div class="mt2"><i class="fa fa-file-powerpoint-o fa-5x"></i></div>';
				}
				$('<div class="attachment" data-id="'+e.id+'"><div class="attachment-preview"><div class="thumbnail">'+mediaitem+'</div><a class="removeSelectedImage" href="javascript:void(0)" title="Deselect"><div class="media-modal-icon"></div></a></div></div>').appendTo(featuredHolder);
				_featuredArr.push(e.id+'');
				featuredOnId.val(_featuredArr);
			}
		});
	}
	
	$('body').on('click', 'a.removeSelectedImage', function(){
		deleteFeaturedItem($(this),'#gallery-textfield');
	});
	
	deleteFeaturedItem = function($item, $textid){
		var deleteFeaturedArr,
			attachment = $item.parents('div.attachment'),
			thisid = attachment.data('id'),
			deleteFeatured = $($textid),
			_deleteFeaturedArr = [],
			arrStringify = "";
		
		if(deleteFeatured.val()[deleteFeatured.val().length-1] === "," ){
			deleteFeaturedArr = deleteFeatured.val().slice(0,-1).split(',');
		}else if(deleteFeatured.val() == "" || deleteFeatured.val() == ","){
			deleteFeatured = [];
		}else{
			deleteFeaturedArr = deleteFeatured.val().split(',');
		}
		
		deleteFeaturedArr[$.inArray(thisid+'', deleteFeaturedArr)] = null;
		$.each(deleteFeaturedArr, function(i,e){
			if(e !== null && e != "" && e != " "){
				if(i == deleteFeaturedArr.length){
					arrStringify+=e;
				}else{
					arrStringify+=e+',';
				}
			}
			
		});
		deleteFeatured.val(arrStringify);
		attachment.remove();
		
	}
	
	//maxFile digunakan jika ada batas penggunaan pada box maxFile
	//options digunakan untuk simpan data id gambar yang saat ini akan digunakan
	function loadFeatured(maxFile, options, filterType, dataCalledArr){
		if(maxFile > 1){
			mediaMaxFiles = maxFile;
		}
		
		if(options){
			mediaOptions = options;
		}
		
		if(!filterType){
			filterType = 'load';
		}
		
		if(dataCalledArr){
			mediaDataCalledArr = dataCalledArr;
		}
		
		$.ajax({
			type: 'POST',
			url: 'kelola.php?r='+mediaPostType+'/mediaajax',
			dataType: 'json',
			data: {ajaxCall: filterType, dataCalled: dataCalledArr},
			success:	function(data){
				removeMediaItem();
				mediaList = data.data;
				$.each(data.data, function(i,e){
					mediaitemView(e);
				});
			},
			error:	function(data){},
			complete: function(){
				NProgress.done();
			},
			beforeSend: function(data){
				NProgress.start();
			}
		});
	}
	
	//filter type = load && filter;
	//dataCalledArr digunakan jika filter type = filter;
	function loadMedia(filterType, dataCalledArr){
		$.ajax({
			type: 'POST',
			url: 'kelola.php?r='+mediaPostType+'/mediaajax',
			dataType: 'json',
			data: {ajaxCall: filterType, dataCalled: dataCalledArr},
			success:	function(data){
				removeMediaItem();
				mediaList = data.data;
				$.each(data.data, function(i,e){
					mediaitemView(e);
				});
			},
			error:	function(data){},
			complete: function(){
				NProgress.done();
			},
			beforeSend: function(data){
				NProgress.start();
			}
		});
	}
	
	function exitMedia(){
		$('#mediaHolderTruly').hide().removeClass('featured');
		var dateFilter = $('#dateFilter'),
			typeFilter = $('#typeFilter');
		dateFilter.val("");
		typeFilter.val("");
		typeFilter.trigger("chosen:updated");
		dateFilter.trigger("chosen:updated");
		deleteDetails();
		removeMediaItem();
		isFeatured = false;
	}

    $.fn.isBefore = function(sel){
        return this.nextAll().filter(sel).length !== 0;
    }
	
	var removeMediaItem = function(){
		$('#media-item-list').children('li').remove();
	}
	
	//Fungsi hapus div media-details
	var deleteDetails = function(){
		$('.media-details').remove();
	}

	function insertAtCaret(element, text) {
		var caretPos = element[0].selectionStart,
		currentValue = element.text();

		element.text(currentValue.substring(0, caretPos) + text);
	}
	
	var grabFilterdata = function(){
		var dateFilterer = $('#dateFilter').val(),
			typeFilterer = $('#typeFilter').val(),
			nameFilterer = $('#nameFilter').val();
		
		return {'date':dateFilterer,'type':typeFilterer,'name':nameFilterer};
	}
	
	var updateMediaItem = function(){
		selectedArr = [];
		detailedArr = {};
		$('li.attachment.selected').each(function(i,e){
			var thisdataid = $(this).data("id");
			$.each(mediaList, function(i,el){
				if(el.id == thisdataid){
					selectedArr.push({
						'id': thisdataid,
						'path':el.attribute.path,
						'filename':el.attribute.filename,
						'type':el.attribute.type,
						'title':el.attribute.title,
						'attribute':el.attribute.attribute,
						'justfilename': el.attribute.justfilename
					});
					
					detailedArr = {
						'id': thisdataid,
						'path':el.attribute.path,
						'filename':el.attribute.filename,
						'type':el.attribute.type,
						'title':el.attribute.title,
						'attribute':el.attribute.attribute,
						'justfilename': el.attribute.justfilename
					};
					
				}
			});
		});
	}
	
	function loadImageSelectBox(json){
		var	stringify = '<select id="details-size" class=""><option value="">Pilih Ukuran</option>';
		$.each(JSON.parse(json).thumbnailSize, function(i,e){
		var activeornot = '';
			if(JSON.parse(json).active == i){
				activeornot = 'selected="selected"';
			}
				
			stringify += '<option value="'+i+'" '+activeornot+'>'+i+' - '+$(this)[0].width+' X '+$(this)[0].height+'</option>';
		});
		stringify += '</select>';
		return stringify;
	}
	
	//Fungsi load details, dipanggil jika media-list item punya class selected
	var loadDetails = function(thisid){
		var rightmediapanel = $('#rightMediaLibraryPanelDetails');
		$.each(mediaList, function(i,e){
			if(e.id == thisid){
				var mediaitem = '<div class="mt2"><i class="fa fa-file fa-5x"></i></div>'
					mediaImage = "";
				
				var mediaHTML = $('<div class="pl1 media-details"><h3 class="fs12 fbold uppercase p1">Detail Attachment</h3><div class="attachment-info">'+mediaitem+'<div class="attachment-details"><div class="filename">'+e.attribute.title+'</div><div class="uploaded">'+e.attribute.date+'</div><a class="delete-attachment" href="#" data-id="'+e.id+'">Hapus</a></div></div><label class="setting" data-setting="title"><span>Judul</span><input id="details-title" type="text" value="'+$(this)[0].attribute.title+'"></label><label class="setting" data-setting="caption"><span>Caption</span><textarea id="details-caption">'+e.attribute.caption+'</textarea></label><label class="setting" data-setting="description"><span>Deskripsi</span><textarea id="details-description">'+e.attribute.description+'</textarea></label><a href="#" class="btn btn-info pull-right mr2 mt05 mb05" id="save-data" data-id="'+thisid+'">Simpan</a>'+mediaImage+'</div>');
				
				if($.inArray(e.attribute.type,pdfType) !== -1){
					mediaitem = '<div class="attachment-thumbnail"><i class="fa fa-file-pdf-o fa-5x"></i></div>';
				}
				else if($.inArray(e.attribute.type,imageType) !== -1){
					mediaitem = '<div class="attachment-thumbnail"><img src="'+$(this)[0].attribute.path+$(this)[0].attribute.filename+'"></div>';
					mediaImage = '<label class="setting" data-setting="image-sizeds"><span>Ukuran</span>'+loadImageSelectBox($(this)[0].attribute.attribute)+'</label>';
					
					mediaItemWidth = JSON.parse(e.attribute.attribute).normalSize[0],
					mediaItemHeight = JSON.parse(e.attribute.attribute).normalSize[1];
					
					mediaHTML = $('<div class="pl1 media-details"><h3 class="fs12 fbold uppercase p1">Detail Attachment</h3><div class="attachment-info">'+mediaitem+'<div class="attachment-details"><div class="filename">'+e.attribute.title+'</div><div class="uploaded">'+e.attribute.date+'</div><div class="dimensions">'+mediaItemWidth+' � '+mediaItemHeight+'</div><a class="delete-attachment" href="#" data-id="'+e.id+'">Hapus</a></div></div><label class="setting" data-setting="title"><span>Judul</span><input id="details-title" type="text" value="'+$(this)[0].attribute.title+'"></label><label class="setting" data-setting="caption"><span>Caption</span><textarea id="details-caption">'+e.attribute.caption+'</textarea></label><label class="setting" data-setting="description"><span>Deskripsi</span><textarea id="details-description">'+e.attribute.description+'</textarea></label><a href="#" class="btn btn-info pull-right mr2 mt05 mb05" id="save-data" data-id="'+thisid+'">Simpan</a>'+mediaImage+'</div>');
				}
				else if($.inArray(e.attribute.type,musicType) !== -1){
					mediaitem = '<div class="attachment-thumbnail"><i class="fa fa-file-sound-o fa-5x"></i></div>';
				}
				else if($.inArray(e.attribute.type,videoType) !== -1){
					mediaitem = '<div class="attachment-thumbnail"><i class="fa fa-file-movie-o fa-5x"></i></div>';
				}
				else if($.inArray(e.attribute.type,compressedType) !== -1){
					mediaitem = '<div class="attachment-thumbnail"><i class="fa fa-file-archive-o fa-5x"></i></div>';
				}
				else if($.inArray(e.attribute.type,wordType) !== -1){
					mediaitem = '<div class="attachment-thumbnail"><i class="fa fa-file-word-o fa-5x"></i></div>';
				}
				else if($.inArray(e.attribute.type,powerpointType) !== -1){
					mediaitem = '<div class="attachment-thumbnail"><i class="fa fa-file-powerpoint-o fa-5x"></div>';
				}
				
				mediaHTML.appendTo(rightmediapanel);
			}
		});
	}
	
	var mediaitemView = function ($item){
		var mediaitem = '<div class="mt2"><i class="fa fa-file fa-5x"></i></div>';
		if($.inArray($item.attribute.type,pdfType) !== -1){
			mediaitem = '<div class="mt2"><i class="fa fa-file-pdf-o fa-5x"></i></div>';
		}
		else if($.inArray($item.attribute.type,imageType) !== -1){
			mediaitem = '<div class="centered"><img src="'+$item.attribute.path+$item.attribute.filename+'" /></div>';
		}
		else if($.inArray($item.attribute.type,musicType) !== -1){
			mediaitem = '<div class="mt2"><i class="fa fa-file-sound-o fa-5x"></i></div>';
		}
		else if($.inArray($item.attribute.type,videoType) !== -1){
			mediaitem = '<div class="mt2"><i class="fa fa-file-movie-o fa-5x"></i></div>';
		}
		else if($.inArray($item.attribute.type,compressedType) !== -1){
			mediaitem = '<div class="mt2"><i class="fa fa-file-archive-o fa-5x"></i></div>';
		}
		else if($.inArray($item.attribute.type,wordType) !== -1){
			mediaitem = '<div class="mt2"><i class="fa fa-file-word-o fa-5x"></i></div>';
		}
		else if($.inArray($item.attribute.type,powerpointType) !== -1){
			mediaitem = '<div class="mt2"><i class="fa fa-file-powerpoint-o fa-5x"></i></div>';
		}
		return $('<li class="attachment" data-id="'+$item.id+'"><div class="attachment-preview"><div class="thumbnail">'+mediaitem+'</div><a class="checkedAttachment" href="javascript:void(0)" title="Deselect"><div class="media-modal-icon"></div></a></div></li>').appendTo($('.media-item-list'));
	}
	
	Dropzone.options.dropzone = {

		autoProcessQueue: true,
		uploadMultiple: true,
		parallelUploads: 100,
		maxFiles: 100,
		addRemoveLinks: false,

		// The setting up of the dropzone
		init: function() {
			var myDropzone = this; 
			this.on("queuecomplete", function(file) {
				$('#rightMediaCartridgeTabs li a[href="#mediaLibrary"]').click();
				$('form#dropzone').removeClass('dz-started').children('.dz-preview').remove();
				$.ajax({
					type: 'POST',
					url: 'kelola.php?r='+mediaPostType+'/mediaajax',
					dataType: 'json',
					data: {ajaxCall: 'load'},
					success:	function(data){
						removeMediaItem();
						mediaList = data.data;
						$.each(data.data, function(i,e){
							mediaitemView(e);
						});
					},
					error:	function(data){
						//window.location=window.location
					},
					complete: function(){
						NProgress.done();
					},
					beforeSend: function(data){
						NProgress.start();
					}
				});
			});
		}
	}
	
	$('body').on('click', '.checkedAttachment', function(){
		var leftMediaLibraryPanel = $('#leftMediaLibraryPanel'),
			leftMediaLibraryPanelChild = $('#leftMediaLibraryPanel li.attachment'),
			thisliAttachment = $(this).parents('li.attachment');
		if(thisliAttachment.hasClass('details')){
			if(thisliAttachment.nextAll('.selected').first().length == 0){
				thisliAttachment.prevAll('.selected').first().addClass('details');
				thisliAttachment.removeClass('selected details');
				deleteDetails();
				loadDetails(thisliAttachment.prevAll('.selected').first().data("id"));
			}
			else{
				thisliAttachment.nextAll('.selected').first().addClass('details');
				thisliAttachment.removeClass('selected details');
				deleteDetails();
				loadDetails(thisliAttachment.nextAll('.selected').first().data("id"));
			}
		}else{
			$('li.details.selected').addClass('donotdeletethis');
			thisliAttachment.removeClass('selected details').parents('ul.media-item-list').find('li.donotdeletethis').addClass('selected details').removeClass('donotdeletethis');
		}
		updateMediaItem();
	});
	
	$(document).keyup(function(e) {
		if (e.keyCode == 27) { exitMedia(); }   // esc
	});
	
	$('body').on('click', 'div.attachment-details a.delete-attachment' ,function(){
		var confirms = confirm("Apakah yakin ingin dihapus?");
		if(confirms){
			$.ajax({
				type: 'POST',
				url: 'kelola.php?r='+mediaPostType+'/deleteattachment',
				dataType: 'json',
				data: {ajaxCall: 'delete', dataCalled: detailedArr},
				success:	function(data){
					if(data){
						loadMedia('filter', grabFilterdata());
						deleteDetails();
					}else if(!data){
						alert("Gambar sedang dipakai");
						loadMedia('filter', grabFilterdata());
						deleteDetails();
					}
				},
				error:	function(data){
					window.location=window.location
				},
				complete: function(){
					NProgress.done();
				},
				beforeSend: function(data){
					NProgress.start();
				}
			});
		}
	});
	
	$('#leftMediaLibraryPanel').on('click', 'div.thumbnail', function(e){
		var leftMediaLibraryPanel = $('#leftMediaLibraryPanel'),
			leftMediaLibraryPanelChild = $('#leftMediaLibraryPanel li.attachment'),
			thisliAttachment = $(this).parents('li.attachment');
		if((e.shiftKey && mediaMaxFiles > 1) || (e.shiftKey && mediaMaxFiles == 0)){
			if(thisliAttachment.hasClass('details')){
				if(leftMediaLibraryPanel.find('li.attachment.details').isBefore(thisliAttachment)){
					leftMediaLibraryPanel.find('li.attachment.details').removeClass('details').nextUntil(thisliAttachment).addClass('selected');
					//Tambah class selected details
					thisliAttachment.addClass('selected details');
					deleteDetails();
					loadDetails(thisliAttachment.data("id"));
				}
				else if(leftMediaLibraryPanel.find('li.attachment.details').isAfter(thisliAttachment)){
					leftMediaLibraryPanel.find('li.attachment.details').removeClass('details').prevUntil(thisliAttachment).addClass('selected');
					//Tambah class selected details
					thisliAttachment.parents('li.attachment').addClass('selected details');
					deleteDetails();
					loadDetails(thisliAttachment.data("id"));
				}
				leftMediaLibraryPanelChild.not(thisliAttachment).removeClass('details');
			}
			else{
				if(leftMediaLibraryPanel.find('li.attachment.details').isBefore(thisliAttachment)){
					leftMediaLibraryPanel.find('li.attachment.details').removeClass('details').nextUntil(thisliAttachment).addClass('selected');
					//Tambah class selected details
					thisliAttachment.addClass('selected details');
					deleteDetails();
					loadDetails(thisliAttachment.data("id"));
				}
				else if(leftMediaLibraryPanel.find('li.attachment.details').isAfter(thisliAttachment)){
					leftMediaLibraryPanel.find('li.attachment.details').removeClass('details').prevUntil(thisliAttachment).addClass('selected');
					//Tambah class selected details
					thisliAttachment.addClass('selected details');
					deleteDetails();
					loadDetails(thisliAttachment.data("id"));
				}
				else{
					leftMediaLibraryPanel.find('li.attachment.details').removeClass('details');
					//Tambah class selected details
					thisliAttachment.addClass('selected details');
					deleteDetails();
					loadDetails(thisliAttachment.data("id"));
				}
			}
			updateMediaItem();
		}
		else if((e.ctrlKey && mediaMaxFiles > 1) || (e.ctrlKey && mediaMaxFiles == 0)){
			if(thisliAttachment.hasClass('details')){
				leftMediaLibraryPanel.find('li.attachment.details').removeClass('details');
				//Hapus class selected details
				thisliAttachment.removeClass('selected details');
				if(thisliAttachment.nextAll('.selected').first().length == 0){
					thisliAttachment.prevAll('.selected').first().addClass('details');
					deleteDetails();
					loadDetails(thisliAttachment.prevAll('.selected').first().data("id"));
				}
				else{
					thisliAttachment.nextAll('.selected').first().addClass('details');
					deleteDetails();
					loadDetails(thisliAttachment.nextAll('.selected').first().data("id"));
				}
			}
			else{
				leftMediaLibraryPanel.find('li.attachment.details').removeClass('details');
				//Tambah class selected details
				thisliAttachment.addClass('selected details');
				leftMediaLibraryPanelChild.not(thisliAttachment).removeClass('details');
				thisliAttachment.addClass('details');
				deleteDetails();
				loadDetails(thisliAttachment.data("id"));
			}
			updateMediaItem();
		}
		else if(thisliAttachment.hasClass('selected')){
			if(thisliAttachment.hasClass('details')){
				thisliAttachment.removeClass('selected details');
				if(thisliAttachment.nextAll('.selected').first().length == 0){
					thisliAttachment.prevAll('.selected').first().addClass('details');
					deleteDetails();
					loadDetails(thisliAttachment.prevAll('.selected').first().data("id"));
				}
				else{
					thisliAttachment.nextAll('.selected').first().addClass('details');
					deleteDetails();
					loadDetails(thisliAttachment.nextAll('.selected').first().data("id"));
				}
			}
			else{
				//Hapus class selected details
				leftMediaLibraryPanelChild.not(thisliAttachment).removeClass('details');
				thisliAttachment.addClass('details');
				deleteDetails();
				loadDetails(thisliAttachment.data("id"));
			}
			updateMediaItem();
		}else{
			leftMediaLibraryPanelChild.not(thisliAttachment).removeClass('selected details');
			//Tambah class selected details
			thisliAttachment.addClass('selected details');
			deleteDetails();
			loadDetails(thisliAttachment.data("id"));
			updateMediaItem();
		}
	});
	
	$('#insert-post').click(function(e){
		e.preventDefault();
		if(!isFeatured){
			var buildString  = "",
				buildStringFeatured  = "",
				buildStringFeaturedId  = "",
				buildStringFeaturedGalleryId  = $('#Post_media_gallery').val();
			if($('#insertMedia').hasClass('active')){
				$(selectedArr).each(function(i,el){
					if($.inArray(el.type,imageType) !== -1){
						var width = "";
						var height = "";
						var imagesized = JSON.parse(el.attribute).active;
						jsonpars = false;
						$.each(JSON.parse(el.attribute).thumbnailSize, function(i,e){
							if(i == imagesized){
								jsonpars = true;
								width = this.width;
								height = this.height;
							}
						});
						if(jsonpars){
							buildString += '<p><img src="'+el.path+el.justfilename+'_'+width+'-'+height+'.'+el.type+'" title="'+el.title+'" alt="'+el.title+'"></p>';
						}
						else{
							buildString += '<p><img src="'+el.path+el.filename+'" title="'+el.title+'" alt="'+el.title+'"></p>';
						}
					}
					else{
						buildString += '<p><a href="'+el.path+el.filename+'" title="'+el.title+'">'+el.title+'</a></p>';
					}
				});
			}
			else if($('#insertUrl').hasClass('active')){
				var urlLinkValue = $('#urlLinkValue'),
					urlLinkTitle = $('#urlLinkTitle');
				buildString+= '<p><a href="'+urlLinkValue.val()+'">'+urlLinkTitle.val()+'</a></p>';
				urlLinkValue.val("");
				urlLinkTitle.val("");
			}
			
			$('.yiis-redactor').redactor('insertHtml', buildString);
		}
		else if(isFeatured){
			insertFeatured();
		}
		
		exitMedia();
	});
	
	$('body').on('click', '#save-data', function(){
		var idtext = $(this).data("id"),
			savearray = [],
			titletext = $('#details-title').val(),
			descriptiontext = $('#details-description').val(),
			captiontext = $('#details-caption').val();
		//Looping mediaList
		$.each(mediaList, function(i,e){
			//Cari item dari medialist yang cocok dengan data-id dari tombol #save-data
			if(e.id == idtext){
				e.attribute.title =  titletext;
				e.attribute.description = descriptiontext;
				e.attribute.caption = captiontext;
				//Masukkan value dari input, textarea yang ada di div details ke dalam array
				savearray = {
					'id': idtext,
					'title': titletext,
					'description': descriptiontext,
					'caption': captiontext,
				};
			}
		});
		//Kirim ajax
		$.ajax({
			type: 'POST',
			url: 'kelola.php?r='+mediaPostType+'/savemedia',
			dataType: 'json',
			data: {ajaxCall: 'save',dataCalled: savearray},
			success:	function(data){},
			error:	function(data){
				window.location=window.location
			},
			complete: function(){
				NProgress.done();
			},
			beforeSend: function(data){
				NProgress.start();
			}
		});
	});
	
	$('#formFilter').submit(function(){
		loadMedia('filter', grabFilterdata());
	});
	
	//Filter Galeri diganti
	$('#formFilter').on('change', 'select', function(){
		loadMedia('filter', grabFilterdata());
	});
	
	$('#mediaFaker').click(function(e){
		exitMedia();
	});
	
	$('#closeMedia').click(function(e){
		exitMedia();
	});
	
	$('#load-data').click(function(){
		showMedia('load');
	});
	
	$('body').on('change', '#details-size', function(){
		ajaxed = $(this).val();
		ajaxedid = $(this).parents('.setting').prev('a').data("id");
		$.each(mediaList, function(i,e){
			if(e.id == ajaxedid){
				ajaxable = JSON.parse(e.attribute.attribute);
				ajaxable.active = ajaxed;
				e.attribute.attribute = JSON.stringify(ajaxable);
			}
		});
		
		$.each(selectedArr, function(i,e){
			if(e.id == ajaxedid){
				ajaxable = JSON.parse(e.attribute);
				ajaxable.active = ajaxed;
				e.attribute = JSON.stringify(ajaxable);
			}
		});
	});
	
});