$(document).ready(function(){

	$('.edit-caption').on('click', function(){
		zx = $(this).parents('.row').find('.caption-title').attr('data-text');
		zc = $(this).parents('.row').find('.caption-desc').attr('data-text');
		zxzcc = $(this).attr('data-id');
		$('.modal .modal-title').text("Edit Caption");
		$('.modal .save-caption').attr('data-id',zxzcc);
		$('.modal .caption-title').val(zx);
		$('.modal .caption-desc').val(zc);
		$('.modal').modal({show: true});
	});
	
	$('.save-caption').click(function(){
		az = $(this).attr('data-id'),
		ss = $(this).attr('data-modul'),
		b = $('.modal .caption-title').val(),
		c = $('.modal .caption-desc').val();
		$.ajax({
			type:"POST",
			url: 'kelola.php?r='+ss+'/AjaxEditCaption',
			dataType: 'json',
			data: {editCaption: az, editCaptions: b, editCaptionss: c },
			success: function(data){
				if(data.a){
					$('div [data-diff='+az+']').find('.caption-title').text(b);
					$('div [data-diff='+az+']').find('.caption-title').attr('data-text', b);
					$('.modal').modal("hide");
				}
				else{
					$('.modal .modal-title').text("Ada yang salah");
					$('.modal').modal({show: true});
				}
			},
		});
	});
	
	
	$('.delete-caption').click(function(){
		zx = confirm("Are you sure?");
		if(zx){
			a = $(this).attr('data-id'),
			ss = $(this).attr('data-modul'),
			b = $(this).closest('div [data-diff='+a+']');
			asx = $('.post.just-once').length;
			asd = $('.download.just-once').length;
			$.ajax({
				type:"POST",
				url: 'kelola.php?r='+ss+'/AjaxEditCaption',
				dataType: 'json',
				data: {deleteAjax: a},
				success: function(data){
					if(data){
						b.remove();
						if(asx == 1){
							$('.btn.btn-success.fileinput-button').removeClass('disabled');
							$('.btn.btn-success.fileinput-button input').attr('disabled',false);
						}
						if(asd <= 2){
							$('.btn.btn-success.fileinput-button').removeClass('disabled');
							$('.btn.btn-success.fileinput-button input').attr('disabled',false);
						}
					}
					else{
						$('.modal .modal-title').text("Ada yang salah");
						$('.modal').modal({show: true});
					}
				},
			});
		}
	});
	
	$('body').on('click', '.uploadsors', function(){
		asdd = $('.post.just-once').length;
		if(asdd >= 1){
			$('.btn.btn-success.fileinput-button').addClass('disabled');
			$('.btn.btn-success.fileinput-button input').attr('disabled',true);
		}
	});
	
});