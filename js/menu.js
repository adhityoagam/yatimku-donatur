jQuery(document).ready(function(){
	urlNow = window.location.href;
	hrefPlus = window.location.origin;
	jQuery('ul.sidebar-nav li a').each(function(){
		thisHref = jQuery(this).attr('href');
		if(hrefPlus+thisHref == urlNow || hrefPlus+thisHref+"/index" == urlNow || hrefPlus+thisHref+"/update" == urlNow.split('&')[0]){
			jQuery(this).addClass('active').parents('li').addClass('active');
			if(jQuery(this).closest('ul').children('li.active').length > 1){
				jQuery(this).closest('ul').children('li.active').first().removeClass('active').children('a').removeClass('active');
			}
		}
	});
});