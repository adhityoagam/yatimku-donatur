$(document).ready(function(){
	
	
	$('#NNested-holder .btn-block-toggle').click(function(){
		if($(this).hasClass('active')){
			$(this).removeClass('active').children().removeClass('fa-chevron-up').addClass('fa-chevron-down');
			$(this).closest('.nested-childest-block').removeClass('nested-active').children('.block-title').removeClass('nested-active').siblings('.row').children('.block-content').stop().hide();
		}else{
			$(this).addClass('active').children().removeClass('fa-chevron-down').addClass('fa-chevron-up');
			$(this).closest('.nested-childest-block').addClass('nested-active').children('.block-title').addClass('nested-active').siblings('.row').children('.block-content').stop().show();
		}
	});	

	$('select').chosen({width: "100%"});
});